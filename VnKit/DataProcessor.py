from VnKit.Lang import VnLang, Char
from collections import Counter
import os
import time
import re

from VnKit.Utils import build_trie


class DataAnalytic(object):
    def __init__(self, processor):
        self.processor = processor

    def analyze_syllable(self, i_path, mode='file'):
        if mode == 'file':
            start = time.time()
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            output_dir = os.path.join(dir_name, i_path.split('.')[0] + '_output')
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)

            print('Process file: {0}'.format(base_name))
            counter = Counter()
            with open(i_path, 'r', encoding='utf8') as f:
                lines = f.read().split('\n')
                for i, line in enumerate(lines):
                    # line = line.lower()
                    if i % 10000 == 0:
                        print("Process at line {0}".format(i + 1))
                    for w in line.split(' '):
                        counter[w] += 1

                all_syllables = sorted(list(counter.items()), key=lambda s: s[1], reverse=True)

                # Analyzie correct and incorrect syllables
                correct_syllables = []
                incorrect_syllables = []

                for s in all_syllables:
                    if self.processor.vn_lang.syllable.is_telex_syllable(
                            self.processor.vn_lang.syllable.any_to_telex(s[0])):
                        correct_syllables.append(s)
                    else:
                        incorrect_syllables.append(s)

                correct_syllables = sorted(correct_syllables, key=lambda s: s[1], reverse=True)
                incorrect_syllables = sorted(incorrect_syllables, key=lambda s: s[1], reverse=True)
                correct_syllables_len = len(correct_syllables)
                incorrect_syllables_len = len(incorrect_syllables)
                with open(os.path.join(output_dir, 'correct_syllables.txt'), 'w', encoding='utf8') as f:
                    for i, s in enumerate(correct_syllables):
                        if i == correct_syllables_len - 1:
                            f.write(s[0] + '==' + str(s[1]))
                        else:
                            f.write(s[0] + '==' + str(s[1]) + '\n')
                with open(os.path.join(output_dir, 'incorrect_syllables.txt'), 'w', encoding='utf8') as f:
                    for i, s in enumerate(incorrect_syllables):
                        if i == incorrect_syllables_len - 1:
                            f.write(s[0] + '==' + str(s[1]))
                        else:
                            f.write(s[0] + '==' + str(s[1]) + '\n')

            end = time.time()
            print('Output dir: {0}'.format(output_dir))
            print('Processing Time : {0} s'.format((end - start)))


class DataExtractor(object):
    URL_REGEX = r"""(?i)\b((?:(https|http)?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b/?(?!@)))"""
    EMAIL_REGEX = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                              "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                              "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

    DATE_REGEX = re.compile('(\d+[-/]\d+[-/]\d+)')
    # MONEY_REGEX = money = re.compile('|'.join([
    #     r'[\$|£](\d*\.\d{1,2})',  # e.g., $.50, .50, $1.50, $.5,
    #     r'[\$|£](\d+)',  # e.g., $500, $5
    #     r'[\$|£](\d+\.?)',  # e.g., $5.
    #     r'[1-9][0-9.,]*\s*(dong|đồng|d|đ)?'
    # ]))
    MONEY_REGEX = r'\s[1-9][0-9.,]*(\sdong|\sđồng|\s*d\s|\s*đ\s)'

    # MONEY_REGEX_3 = r'[1-9][0-9.,]*\s*[d|đ]?'

    def __init__(self, processor):
        self.processor = processor

    def get_emails(self, s):
        return [email[0] for email in re.findall(DataExtractor.EMAIL_REGEX, s) if not email[0].startswith('//')]

    def get_urls(self, s):
        return [url[0] for url in re.findall(DataExtractor.URL_REGEX, s)]


class DataProcessor(object):
    def __init__(self):
        self.vn_lang = VnLang()
        self.analytic = DataAnalytic(self)
        self.extractor = DataExtractor(self)
        self.init_address()

    def init_address(self):
        with open('/home/user/Projects/VietnameseDiacriticsRestoration/VnKit/kit_data/address.txt', 'r',
                  encoding='utf8') as f:
            lines = f.read().split('\n')
            pxs = set()
            qhs = set()
            tps = set()
            for l in lines:
                l = l.split(',')
                px = re.sub('(Phường |Xã |Thị trấn )', '', l[0])
                qh = re.sub('(Quận |Huyện |Thành phố )', '', l[1])
                tp = re.sub('(Thành phố |Tỉnh )', '', l[2])
                pxs.add(px)
                qhs.add(qh)
                tps.add(tp)
            pxs = list(pxs)
            qhs = list(qhs)
            tps = list(tps)
            self.locations = pxs + qhs + tps
            self.locations = [self.vn_lang.remove_diacritics(l.lower()) for l in self.locations]
            self.location_trie = build_trie(self.locations)

    def is_location(self, loc):
        return self.location_trie.is_valid_word(self.vn_lang.remove_diacritics(loc.lower()))

    def clean_data(self, data):
        # Remove HTML
        x = re.sub("<(.*?)>", "", data)
        x = re.sub("&[a-z]*;", "", x)
        x = re.sub('…', '.', x)
        # Remove Out Of Vocab Character

        full_char = ''.join(Char.FULL_CHARACTERS)
        full_char += full_char.upper()
        full_char += '0123456789'
        x = re.sub('[^' + full_char + ' \@\%\:\-\/\,\;\.\?\!\(\)]', "", x)

        x = re.sub('\.+', ".", x)
        x = re.sub('\?+', "?", x)

        # for w in re.findall('[0-9]+\.[^0-9]', x):
        #     _w = re.sub('\.', ' . ', w)
        #     x = re.sub(w, _w, x)
        for w in re.findall('[0-9]+\.[0-9]', x):
            _w = re.sub('\.', ',', w)
            x = re.sub(w, _w, x)

        for w in re.findall('[0-9]+-[0-9]', x):
            _w = w.replace('-', '/')
            x = re.sub(w, _w, x)

        x = re.sub('\s+', ' ', x)
        return x

    def normalize_data(self, data):
        output = ''
        data_len = len(data)
        for i, c in enumerate(data):
            if c in ':;.?!()':
                output += ' ' + c + ' '
            elif c == ',':
                if (i > 0 and not self.vn_lang.char.is_number(data[i - 1])) or (
                                i < data_len - 1 and not self.vn_lang.char.is_number(data[i + 1])):
                    output += ' ' + c + ' '
                else:
                    output += c
            elif c == '-':
                if (i > 0 and not self.vn_lang.char.is_char(data[i - 1])) or (
                                i < data_len - 1 and not self.vn_lang.char.is_char(data[i + 1])):
                    output += ' ' + c + ' '
                else:
                    output += c

            elif c == '/':
                if 0 < i and i < data_len - 1 and data[i - 1] == ' ' and data[i + 1] == ' ':
                    pass
                else:
                    output += c
            else:
                output += c
        output = output.strip()
        output = re.sub('\s+', ' ', output)

        return output

    def tag_data(self, data):
        x = re.sub(DataExtractor.URL_REGEX, '<url>', data)
        # print(x)
        x = re.sub(DataExtractor.EMAIL_REGEX, '<email>', x)
        # print(x)
        x = re.sub(DataExtractor.DATE_REGEX, '<date>', x)
        # print(x)
        x = re.sub('\d+[%]', '<percentnumber>', x)
        # print(x)

        x = x.split(' ')
        for i, s in enumerate(x):
            if s.isdigit():
                if i > 0:
                    if x[i - 1] in ['ngày', 'tháng', 'năm', 'ngay', 'thang', 'nam']:
                        x[i] = '<date>'
                    elif x[i - 1] in ['gia', 'giá']:
                        x[i] = '<money>'
                    else:
                        x[i] = '<number>'

        x = ' '.join(x)

        x = re.sub(DataExtractor.MONEY_REGEX, ' <money> ', x)

        return x

    def process_data(self, data, tagging=False):
        x = self.clean_data(data)
        if tagging:
            x = self.tag_data(x)
        x = self.normalize_data(x)
        if tagging:
            x = self.tag_data(x)
        return x

    def normalize_data_step_2(self, sentence):
        sentence = sentence.split(' ')
        new_ouput = ''
        for i, s in enumerate(sentence):

            if i > 0:
                if (s.istitle() or s.isupper()) and (sentence[i - 1].istitle() or sentence[i - 1].isupper()):
                    new_ouput += '_' + s
                else:
                    new_ouput += ' ' + s
            else:
                new_ouput += s

        return new_ouput

    def tag_data_step_2(self, data):
        data = data.split(' ')
        for i, s in enumerate(data):
            if i > 0:
                # if '-' in s or '_' in s or s.istitle() or s.isupper():
                if '-' in s or '_' in s:
                    s = re.sub('[\_|\-]', ' ', s)
                    if self.is_location(s):
                        data[i] = '<location>'
                    else:
                        data[i] = '<name>'
            else:
                if '-' in s or '_' in s:
                    s = re.sub('[\_|\-]', ' ', s)
                    if self.is_location(s):
                        data[i] = '<location>'
                    else:
                        data[i] = '<name>'

        data = ' '.join(data)
        return data

    def process_data_step_2(self, sentence, tagging=False):
        x = self.normalize_data_step_2(sentence)
        if tagging:
            x = self.tag_data_step_2(x)
        return x

    def clean_file(self, i_path, o_path=None):
        print("Process file:", i_path)
        start = time.time()
        if o_path is None:
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            o_path = os.path.join(dir_name, 'clean_' + base_name)

        with open(i_path, 'r', encoding='utf8') as f_in:
            with open(o_path, 'w', encoding='utf8') as f_out:
                lines = f_in.read().split('\n')
                lines_len = len(lines)
                for i, line in enumerate(lines):
                    if i % 10000 == 0:
                        print(i)
                    if i == lines_len - 1:
                        f_out.write(self.process_data(line))
                    else:
                        f_out.write(self.process_data(line) + '\n')
        end = time.time()
        print("Output file: ", o_path)
        print("Processing Time: {0} s".format(end - start))
        return o_path

    def sentence_split_file(self, i_path, o_path=None):
        print("Process file:", i_path)
        if o_path is None:
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            o_path = os.path.join(dir_name, 'sen_split_' + base_name)

        is_first = True
        with open(i_path, 'r', encoding='utf8') as f_in:
            with open(o_path, 'w', encoding='utf8') as f_out:
                lines = f_in.read().split('\n')
                for line in lines:
                    if len(line) == 0:
                        continue
                    sentences = self.sentence_split(line)
                    for sen in sentences:
                        if len(sen.split(' ')) <= 5:
                            continue
                        if is_first:
                            f_out.write(sen)
                            is_first = False
                        else:
                            f_out.write('\n' + sen)

        return o_path

    def process_file(self, i_path, o_path=None, is_remove=False):
        print("Process file:", i_path)
        if o_path is None:
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            o_path = os.path.join(dir_name, 'final_' + base_name)

        is_first = True
        with open(i_path, 'r', encoding='utf8') as f_in:
            with open(o_path, 'w', encoding='utf8') as f_out:
                lines = f_in.read().split('\n')
                for line in lines:
                    new_line = self.process_data_step_2(line)
                    filter_line, is_vn = self.filter_vn(new_line)
                    if is_vn == True:
                        if is_first:
                            f_out.write(filter_line)
                            is_first = False
                        else:
                            f_out.write('\n' + filter_line)
        return o_path

    # def sentence_split_file(self, i_path, o_path):
    #     print("Process file:", i_path)
    #     with open(i_path, 'r', encoding='utf8') as f_in:
    #         with open(o_path, 'w', encoding='utf8') as f_out:
    #             lines = f_in.read().split('\n')
    #             for line in lines:
    #                 if len(line) == 0:
    #                     continue
    #                 sentences = self.sentence_split(line)
    #                 for sen in sentences:
    #                     sen = sen.split(' ')
    #                     if is_remove:
    #                         new_sen = []
    #                         for i, syllable in enumerate(sen):
    #                             if self.vn_lang.syllable.is_syllable(
    #                                     syllable.lower()) or syllable in VnLang.PUNCTUATIONS:
    #                                 new_sen.append(syllable)
    #                             elif syllable.istitle():
    #                                 new_sen.append(syllable)
    #                             else:
    #                                 pass
    #
    #                         if len(new_sen) > 1:
    #                             f_out.write(' '.join(new_sen) + '\n')
    #                     else:
    #                         if len(sen) > 1:
    #                             f_out.write(' '.join(sen) + '\n')

    # def process_sequence(self, sequence, keep_case_sensitive=True):
    #     sequence_len = len(sequence)
    #     output = ''
    #
    #     for i, char in enumerate(sequence):
    #         lower_char = char.lower()
    #         if lower_char == ' ':
    #             if len(output) > 0 and output[-1] != u' ':
    #                 output += ' '
    #         elif lower_char in Char.FULL_CHARACTER + Char.NUMBER:
    #             # if len(output) > 0 and output[-1] in PUNCTUATIONS + NUMBER and lower_char not in NUMBER:
    #             #     output += u' ' + char
    #             # else:
    #             #     output += char
    #             output += char
    #
    #         elif lower_char in VnLang.PUNCTUATIONS:
    #             # truong hop 30.000.000d
    #
    #             if len(output) > 0 and output[-1] in Char.NUMBER and i + 1 < sequence_len and sequence[
    #                         i + 1] in Char.NUMBER:
    #                 if char == u'.':
    #                     output += u','
    #                 else:
    #                     output += char
    #             elif len(output) > 0 and output[-1] in Char.FULL_CHARACTER and i + 1 < sequence_len and sequence[
    #                         i + 1] in Char.FULL_CHARACTER and char == u'.':
    #                 output += char
    #             else:
    #                 if len(output) > 0 and output[-1] != ' ':
    #                     output += ' ' + char + ' '
    #                 else:
    #                     output += char
    #         else:
    #             pass
    #
    #     if len(output) > 0 and output[-1] == ' ':
    #         output = output[0:len(output) - 1]
    #
    #     if keep_case_sensitive == False:
    #         return output.lower()
    #     else:
    #         return output

    def sentence_split(self, sequence, punctuations=['.', '?', '!']):
        sentences = []
        sen = []

        for s in sequence.split(' '):
            if s != ' ':
                sen.append(s)
                if s in punctuations:
                    sentences.append(' '.join(sen))
                    sen = []
        return sentences

    def filter_vn(self, sequence):
        words = sequence.split(' ')
        is_vn = True
        for i, w in enumerate(words):
            if self.vn_lang.syllable.is_syllable(w) or \
                            w in self.vn_lang.PUNCTUATIONS or \
                    (w[0] in '0123456789' and w[-1] in '0123456789') or \
                            w in ['<name>',
                                  '<location>',
                                  '<date>',
                                  '<number>',
                                  '<money>',
                                  '<url>',
                                  '<email>'
                                  ] or '_' in w:
                pass
            else:
                is_vn = False
                # print(words[i])
                words[i] = '<unknown>'
        return ' '.join(words), is_vn

    def filter_file(self, i_path, o_path=None):
        print("Process file:", i_path)
        if o_path is None:
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            o_path = os.path.join(dir_name, 'filter_' + base_name)

        is_first = True
        with open(i_path, 'r', encoding='utf8') as f_in:
            with open(o_path, 'w', encoding='utf8') as f_out:
                lines = f_in.read().split('\n')
                for line in lines:
                    filter_line, is_vn = self.filter_vn(line)
                    if is_vn == True:
                        if is_first:
                            f_out.write(filter_line)
                            is_first = False
                        else:
                            f_out.write('\n' + filter_line)
        return o_path

    def telex_file(self, i_path, o_path=None):
        print("Process file:", i_path)
        if o_path is None:
            dir_name = os.path.dirname(i_path)
            base_name = os.path.basename(i_path)
            o_path = os.path.join(dir_name, 'telex_' + base_name)

        with open(i_path, 'r', encoding='utf8') as f_in:
            with open(o_path, 'w', encoding='utf8') as f_out:
                lines = f_in.read().split('\n')
                lines_len = len(lines)

                for i, line in enumerate(lines):
                    filter_line, is_vn = self.filter_vn(line)
                    # print(filter_line)
                    if is_vn == True:
                        if i < lines_len - 1:
                            f_out.write(self.vn_lang.any_to_telex(filter_line) + '\n')
                        else:
                            f_out.write(self.vn_lang.any_to_telex(filter_line))


if __name__ == '__main__':
    dp = DataProcessor()
    # i_path = '/media/user/Others/python3/data_vn/output/corpus.txt'
    # i_path = '/media/user/Others/python3/data_vn/output/truyendai_output.txt'
    # i_path = '/media/user/Others/python3/data_vn/blog/output.txt'
    #
    # #
    # o_path = dp.clean_file(i_path)
    # o_path = dp.sentence_split_file(o_path)
    # # o_path = '/media/user/Others/python3/data_vn/plain/sen_split_clean_vietnamnet.txt'
    #
    # o_path = dp.telex_file(o_path)

    # o_path = dp.process_file(o_path)
    # o_path = dp.filter_file(o_path)
    # print(o_path)
    # dp.analytic.analyze_syllable(o_path, mode='file')


    text = dp.process_data(
        'một nhà thám hiểm đơn thân hơn một người đi du lịch theo tour… tang 3% trong nam 2017... Co 5 nguoi tham gia vao chuyen du lich nay. Gia cua chuyen du lich la 5.000.000d hoac 5.000.000 d hoac 50,000,000 dong',
        tagging=True)
    print(text)
    text = dp.process_data('Hà Nội ngay 18 thang 2 nam 2012', tagging=True)
    print(text)
    text = dp.process_data_step_2(text, tagging=True)
    print(text)
    text = dp.process_data('het 50d $50  50.000.000d 50.000.000 dong', tagging=True)
    print(text)
    # print(dp.clean_data(
    #     '<body>Email cua minh la haimeo94@gmail.com. Website cua minh la:http://dantri.com.vn http://dantri.com.vn 30/04/1975 15-2-2017. Oi dep qua...</body>'))
    # print(dp.extractor.get_emails('Email cua minh la haimeo94@gmail.com haimeo94@gmail.com haimeo94@gmail.com'))
