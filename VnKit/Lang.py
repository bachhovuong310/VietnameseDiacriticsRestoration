import time
import torch

from VnKit.Utils import build_trie, build_dictionary


class CharDictionary:
    CHAR_MORES = [
        ' ', '\x00',
        '<url>', '<email>', '<date>', '<name>', '<location>',
        '<number>', '<percentnumber>', '<money>',
        '<oov>', '<sos>', '<eos>'
    ]

    def __init__(self, case_sensitive=False, has_diacritics=False):
        if not case_sensitive:
            if has_diacritics:
                self.vocab = Char.FULL_CHARACTERS + CharDictionary.CHAR_MORES + VnLang.PUNCTUATIONS
            else:
                self.vocab = Char.BASE_CHARACTERS + CharDictionary.CHAR_MORES + VnLang.PUNCTUATIONS

            key2char, char2key = build_dictionary(self.vocab)
            self.key2char = key2char
            self.char2key = char2key
            self.vocab_len = len(self.vocab)
            print(self.char2key)
        else:
            pass

    @property
    def PAD(self):
        return self.char2key['\x00']

    @property
    def SOS(self):
        return self.char2key['<sos>']

    @property
    def EOS(self):
        return self.char2key['<eos>']

    @property
    def OOV(self):
        return self.char2key['<oov>']

    def encode(self, char_list, seq_length=None, add_seq_tag=False):
        if add_seq_tag:
            char_list = ['<sos>'] + char_list + ['<eos>']

        output = []
        if seq_length is not None:
            if seq_length > len(output):
                char_list += ['\x00'] * (seq_length - len(output))
            else:
                char_list = char_list[:seq_length]

        for c in char_list:
            if c in self.char2key:
                output.append(self.char2key[c])
            else:
                output.append(self.char2key['<oov>'])

        return output


# ### LANGUAGE #####
class Tone:
    NGANG = 0
    HUYEN = 1
    SAC = 2
    HOI = 3
    NGA = 4
    NANG = 5

    TONES = [u'', u'f', u's', u'r', u'x', u'j']
    TONE2INDEX = {
        u'': 0, u'f': 1, u's': 2, u'r': 3, u'x': 4, u'j': 5
    }

    ADDITIONS = ['', 'a', 'e', 'o', 'w', 'ww', 'd', 'da', 'de', 'do', 'dw', 'dww']
    ADDITION_TONES = [
        '', 'a', 'af', 'aj', 'ar', 'as', 'ax', 'd', 'da', 'daf', 'daj', 'dar', 'das', 'dax', 'de', 'def', 'dej', 'der',
        'des', 'dex', 'df', 'dj', 'do', 'dof', 'doj', 'dor', 'dos', 'dox', 'dr', 'ds', 'dw', 'dwf', 'dwj', 'dwr', 'dws',
        'dww', 'dwwf', 'dwwj', 'dwwr', 'dwws', 'dwwx', 'dwx', 'dx', 'e', 'ef', 'ej', 'er', 'es', 'ex', 'f', 'j', 'o',
        'of', 'oj', 'or', 'os', 'ox', 'r', 's', 'w', 'wf', 'wj', 'wr', 'ws', 'ww', 'wwf', 'wwj', 'wwr', 'wws', 'wwx',
        'wx', 'x'
    ]

    def __init__(self, lang):
        self.lang = lang

    def is_tone(self, tone):
        if tone is not None and isinstance(tone, int) and 0 <= tone and tone <= 5:
            return True
        return False

    def is_addition(self, a):
        for add in Tone.ADDITIONS:
            if add == a:
                return True
        return False


class Char:
    NUMBERS = [u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9']
    BASE_CHARACTERS = [
        u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
        u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z'
    ]
    FULL_CHARACTERS = [
        u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
        u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z',
        u'à', u'á', u'â', u'ã', u'è', u'é', u'ê', u'ì', u'í', u'ò', u'ó',
        u'ô', u'õ', u'ù', u'ú', u'ý', u'ă', u'đ', u'ĩ', u'ũ', u'ơ', u'ư',
        u'ạ', u'ả', u'ấ', u'ầ', u'ẩ', u'ẫ', u'ậ', u'ắ', u'ằ', u'ẳ', u'ẵ', u'ặ',
        u'ẹ', u'ẻ', u'ẽ', u'ế', u'ề', u'ể', u'ễ', u'ệ', u'ỉ', u'ị', u'ọ', u'ỏ',
        u'ố', u'ồ', u'ổ', u'ỗ', u'ộ', u'ớ', u'ờ', u'ở', u'ỡ', u'ợ', u'ụ',
        u'ủ', u'ứ', u'ừ', u'ử', u'ữ', u'ự', u'ỳ', u'ỵ', u'ỷ', u'ỹ'
    ]

    BASE_VOWELS = [u'a', u'a', u'a', u'e', u'e', u'i', u'o', u'o', u'o', 'u', 'u', 'y']
    VOWELS = [u'a', u'ă', u'â', u'e', u'ê', u'i', u'o', u'ô', u'ơ', u'u', u'ư', u'y']
    ALL_VOWELS = [
        'a', 'à', 'á', 'ả', 'ã', 'ạ',
        'e', 'è', 'é', 'ẻ', 'ẽ', 'ẹ',
        'i', 'ì', 'í', 'ỉ', 'ĩ', 'ị',
        'o', 'ò', 'ó', 'ỏ', 'õ', 'ọ',
        'u', 'ù', 'ú', 'ủ', 'ũ', 'ụ',
        'y', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ',
        'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ',
        'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ',
        'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ',
        'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ',
        'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ',
        'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'
    ]
    TONES_VOWELS = {
        'a': u'aàáảãạ',
        'ă': u'ăằắẳẵặ',
        'â': u'âầấẩẫậ',
        'e': u'eèéẻẽẹ',
        'ê': u'êềếểễệ',
        'i': u'iìíỉĩị',
        'o': u'oòóỏõọ',
        'ô': u'ôồốổỗộ',
        'ơ': u'ơờớởỡợ',
        'u': u'uùúủũụ',
        'ư': u'ưừứửữự',
        'y': u'yỳýỷỹỵ'
    }
    ADDITION_VOWELS = [u'', u'w', u'a', u'', u'e', u'', u'', u'o', u'w', u'', u'w', u'']

    def __init__(self, lang):
        self.lang = lang

    def is_char(self, c):
        c = c.lower()
        for char in Char.FULL_CHARACTERS:
            if char == c:
                return True
        return False

    def is_number(self, c):
        return c in Char.NUMBERS

    def is_char_or_number(self, c):
        if self.is_char(c):
            return True
        return self.is_number(c)

    def is_vowel(self, c):
        c = c.lower()
        for v in Char.ALL_VOWELS:
            if v == c:
                return True
        return False

    def to_vowel_tone(self, vowel):
        is_upper = vowel.isupper()

        for k in Char.TONES_VOWELS.keys():
            vowels = Char.TONES_VOWELS[k]
            for i in range(len(vowels)):
                if vowel.lower() == vowels[i]:
                    if is_upper:
                        return k.upper(), i
                    else:
                        return k, i

        return vowel, None

    def to_base_vowel(self, c):
        vowel, t = self.to_vowel_tone(c)
        if t is None or t < 0 or t > 5:
            return c

        for i, v in enumerate(Char.VOWELS):
            if v == vowel:
                return Char.BASE_VOWELS[i]

        return c

    def add_a_vowel_tone(self, vowel, tone):
        if not self.lang.tone.is_tone(tone):
            return vowel
        if vowel == '' or tone == 0:
            return vowel
        if not self.is_vowel(vowel):
            return vowel

        uppercase = vowel.isupper()
        vowel = vowel.lower()

        vowel = Char.TONES_VOWELS[vowel][tone]
        if uppercase:
            return vowel.upper()
        return vowel


class Consonant:
    SINGLE_CONSONANTS = [
        u'b', u'c', u'd', u'đ', u'g', u'h', u'k', u'l', u'm', u'n', u'p', u'q', u'r', u's', u't', u'v',
        u'x'
    ]
    DI_CONSONANTS = [u'ch', u'gh', u'gi', u'kh', u'ng', u'nh', u'ph', u'th', u'tr']
    TRI_CONSONANTS = [u'ngh']
    CONSONANTS = SINGLE_CONSONANTS + DI_CONSONANTS + TRI_CONSONANTS

    def __init__(self, lang):
        self.lang = lang

    def is_single_consonant(self, c):
        for con in Consonant.SINGLE_CONSONANTS:
            if con == c:
                return True
        return False

    def is_di_consonat(self, c):
        for con in Consonant.DI_CONSONANTS:
            if con == c:
                return True
        return False

    def is_tri_consonat(self, c):
        for con in Consonant.TRI_CONSONANTS:
            if con == c:
                return True
        return False

    def is_consonant(self, c):
        return self.is_single_consonant(c) or self.is_di_consonat(c) or self.is_tri_consonat(c)


class Rhyme:
    # d r gi
    # c k q  g gh ng ngh du_dau vi_tri_them_dau(chi so cua nguyen am)
    # 0 0 0  0 0  0  0   0
    RULE_RHYMES = {
        # a
        u'a': '10010101',
        u'ai': '10010101',
        u'am': '10010101',
        u'an': '10010101',
        u'ang': '10010101',
        u'anh': '10010101',
        u'ao': '10010101',
        u'au': '10010101',
        u'ay': '10010101',
        u'ac': '10010100',
        u'ach': '10010100',
        u'ap': '10010100',
        u'at': '10010100',
        # # ă
        u'ăm': '10010101',
        u'ăn': '10010101',
        u'ăng': '10010101',
        u'ăc': '10010100',
        u'ăp': '10010100',
        u'ăt': '10010100',
        # â
        u'âm': '10010101',
        u'ân': '10010101',
        u'âng': '10010101',
        u'âu': '10010101',
        u'ây': '10010101',
        u'âc': '10010100',
        u'âp': '10010100',
        u'ât': '10010100',
        # e
        u'e': '01001011',
        u'em': '01001011',
        u'en': '01001011',
        u'eng': '01001011',
        u'eo': '01001011',
        u'ec': '01001010',
        u'ep': '01001010',
        u'et': '01001010',

        # ê
        u'ê': '01001011',
        u'êm': '01001011',
        u'ên': '01001011',
        u'ênh': '01001011',
        u'êu': '01001011',
        u'êch': '01001010',
        u'êp': '01001010',
        u'êt': '01001010',
        # i
        u'i': '01001011',
        u'ia': '01001011',
        u'im': '01001011',
        u'in': '01001011',
        u'inh': '01001011',
        u'iu': '01001011',
        # u'ic': '01001010', ?????????????????
        u'ich': '01001010',
        u'ip': '01001010',
        u'it': '01001010',
        # iê
        u'iêm': '01001011',
        u'iên': '01001011',
        u'iêng': '01001011',
        u'iêu': '01001011',
        u'iêc': '01001010',
        u'iêp': '01001010',
        u'iêt': '01001010',
        # o
        u'o': '10010101',
        # u'oa': '10010101',  # truong hop dac biet
        u'oi': '10010101',
        u'om': '10010101',
        u'on': '10010101',
        u'ong': '10010101',
        u'oc': '10010100',
        u'op': '10010100',
        u'ot': '10010100',
        # oa
        u'oa': '00110101',
        u'oai': '00110101',
        u'oan': '00110101',
        u'oam': '00110101',
        u'oang': '00110101',
        u'oanh': '00110101',
        u'oao': '00110101',
        u'oau': '00110101',
        u'oay': '00110101',
        u'oac': '00110100',
        u'oach': '00110100',
        u'oap': '00110100',
        u'oat': '00110100',
        # oă
        u'oăm': '00110101',
        u'oăn': '00110101',
        u'oăng': '00110101',
        u'oăc': '00110100',
        u'oăt': '00110100',
        u'oăp': '00110100',
        # oe
        u'oe': '00110101',
        u'oem': '00110101',
        u'oen': '00110101',
        # u'oeng': '00110101', ??????????????
        u'oeo': '00110101',
        u'oet': '00110100',
        # oo
        u'oong': '10010101',
        u'ooc': '10010100',
        # ô
        u'ô': '10010101',
        u'ôi': '10010101',
        u'ôm': '10010101',
        u'ôn': '10010101',
        u'ông': '10010101',
        u'ôc': '10010100',
        u'ôp': '10010100',
        u'ôt': '10010100',
        # ơ
        u'ơ': '10010101',
        u'ơi': '10010101',
        u'ơm': '10010101',
        u'ơn': '10010101',
        # u'ơng': '10010101', ?????????????????
        # u'ơc': '10010100', ??????????????????
        u'ơp': '10010100',
        u'ơt': '10010100',
        # u
        u'u': '10010101',
        u'ua': '10010101',
        u'ui': '10010101',
        u'um': '10010101',
        u'un': '10010101',
        u'ung': '10010101',
        u'uy': '10010101',
        u'uc': '10010100',
        u'up': '10010100',
        u'ut': '10010100',
        # uâ
        u'uân': '00110101',
        u'uâng': '00110101',
        u'uây': '00110101',
        u'uât': '00110100',
        # uê
        u'uê': '00110101',
        u'uêu': '00110101',
        u'uênh': '00110101',
        u'uên': '00110101',
        u'uêt': '00110100',
        u'uêch': '00110100',
        # uô
        u'uôi': '10010101',
        u'uôm': '10010101',
        u'uôn': '10010101',
        u'uông': '10010101',
        u'uôc': '10110100',
        u'uôp': '10010100',
        u'uôt': '10010100',
        # uơ
        u'uơ': '00110101',
        # uy
        u'uy': '00110101',
        u'uya': '00110101',
        u'uyn': '00110101',
        u'uynh': '00110101',
        u'uyu': '00110101',
        u'uych': '00110100',
        u'uyt': '00110100',
        # uyê
        u'uyên': '00110101',
        u'uyêt': '00110100',
        # ư
        u'ư': '10010101',
        u'ưa': '10010101',
        u'ưi': '10010101',
        u'ưm': '10010101',
        u'ưn': '10010101',
        u'ưng': '10010101',
        u'ưu': '10010101',
        u'ưc': '10010100',
        # u'ưp': '10010100', ?????????????
        u'ưt': '10010100',
        # ươ
        u'ươi': '10010101',
        u'ươm': '10010101',
        u'ươn': '10010101',
        u'ương': '10010101',
        u'ươu': '10010101',
        u'ươc': '10010100',
        u'ươp': '10010100',
        u'ươt': '10010100',
        # y
        u'y': '01000001',
        # yê
        u'yên': '00000001',
        u'yêm': '00000001',
        u'yêng': '00000001',
        u'yêu': '00000001',
        u'yêt': '00000000',
    }
    BASE_RHYMES = [
        'a', 'ac', 'ach', 'ai', 'am', 'an', 'ang', 'anh', 'ao', 'ap', 'at', 'au', 'ay',
        'e', 'ec', 'ech', 'em', 'en', 'eng', 'enh', 'eo', 'ep', 'et', 'eu',
        'i', 'ia', 'ich', 'iec', 'iem', 'ien', 'ieng', 'iep', 'iet', 'ieu', 'im', 'in', 'inh', 'ip', 'it', 'iu',
        'o', 'oa', 'oac', 'oach', 'oai', 'oam', 'oan', 'oang', 'oanh', 'oao', 'oap', 'oat', 'oau', 'oay', 'oc', 'oe',
        'oem',
        'oen', 'oeo', 'oet', 'oi', 'om', 'on', 'ong', 'ooc', 'oong', 'op', 'ot',
        'u', 'ua', 'uan', 'uang', 'uat', 'uay', 'uc', 'ue', 'uech', 'uen', 'uenh', 'uet', 'ueu', 'ui', 'um', 'un',
        'ung',
        'uo', 'uoc', 'uoi', 'uom', 'uon', 'uong', 'uop', 'uot',
        'uou', 'up', 'ut', 'uu', 'uy', 'uya', 'uych', 'uyen', 'uyet', 'uyn', 'uynh', 'uyt', 'uyu',
        'y', 'yem', 'yen', 'yeng', 'yet', 'yeu'
    ]
    MAX_RHYME_LEN = 5
    NUMBER_RHYMES = len(RULE_RHYMES.keys())

    def __init__(self, lang):
        self.lang = lang
        self.rhyme_trie = build_trie(Rhyme.RULE_RHYMES.keys())
        self.base_rhyme_trie = build_trie(Rhyme.BASE_RHYMES)

    def is_rhyme(self, rhyme):
        rhyme = rhyme.lower()
        if rhyme == '' or len(rhyme) >= Rhyme.MAX_RHYME_LEN:
            return False
        return self.rhyme_trie.is_valid_word(rhyme)

    def is_base_rhyme(self, rhyme):
        rhyme = rhyme.lower()
        if rhyme == '' or len(rhyme) > Rhyme.MAX_RHYME_LEN:
            return False
        return self.base_rhyme_trie.is_valid_word(rhyme)

    def to_rhyme_tone(self, rhyme):
        if rhyme == '' or len(rhyme) >= Rhyme.MAX_RHYME_LEN:
            return rhyme, None

        __rhyme = list(rhyme)

        for i, c in enumerate(__rhyme):
            v, t = self.lang.char.to_vowel_tone(c)
            if t is not None and t > 0:
                __rhyme[i] = v
                __rhyme = ''.join(__rhyme)
                if not self.is_rhyme(__rhyme):
                    return rhyme, None

                return __rhyme, t

        __rhyme = ''.join(__rhyme)

        if not self.is_rhyme(__rhyme):
            return rhyme, None

        return __rhyme, 0

    def add_rhyme_tone(self, rhyme, tone, is_q=False):
        if not self.is_rhyme(rhyme) or not self.lang.tone.is_tone(tone):
            return rhyme

        rhyme_len = len(rhyme)
        if rhyme_len == 0 or tone == 0:
            return rhyme

        __rhyme = list(rhyme)
        nb_vowel = 0
        for c in __rhyme:
            if c.lower() in Char.VOWELS:
                nb_vowel += 1
            else:
                break

        if nb_vowel == 0:
            return rhyme

        if nb_vowel == 1:
            toned_vowel = __rhyme[0]
            if self.lang.char.is_vowel(toned_vowel):
                __rhyme[0] = self.lang.char.add_a_vowel_tone(toned_vowel, tone)
                return ''.join(__rhyme)

        for i in reversed(range(rhyme_len)):
            c = __rhyme[i]
            if c.lower() in u'ăâêôơư':
                __rhyme[i] = self.lang.char.add_a_vowel_tone(c, tone)
                return ''.join(__rhyme)

        if nb_vowel == 2:
            if __rhyme[-1] not in Char.VOWELS:
                toned_vowel = __rhyme[1]
                __rhyme[1] = self.lang.char.add_a_vowel_tone(toned_vowel, tone)
                return ''.join(__rhyme)

            if is_q:
                toned_vowel = __rhyme[1]
                __rhyme[1] = self.lang.char.add_a_vowel_tone(toned_vowel, tone)
                return ''.join(__rhyme)

            toned_vowel = __rhyme[0]
            __rhyme[0] = self.lang.char.add_a_vowel_tone(toned_vowel, tone)
            return ''.join(__rhyme)

        if nb_vowel == 3:
            toned_vowel = __rhyme[1]
            __rhyme[1] = self.lang.char.add_a_vowel_tone(toned_vowel, tone)
            return ''.join(__rhyme)

        # Phong truong hop sai
        return rhyme

    def to_base_rhyme_addition(self, rhyme):
        if not self.is_rhyme(rhyme):
            return rhyme, None

        addition = u''
        rhyme = list(rhyme)
        vowels_len = len(Char.VOWELS)
        for i in range(len(rhyme)):
            for j in range(vowels_len):
                if rhyme[i] == Char.VOWELS[j]:
                    rhyme[i] = Char.BASE_VOWELS[j]
                    addition += Char.ADDITION_VOWELS[j]
                    break

        return ''.join(rhyme), addition

    def add_base_rhyme_addition(self, base_rhyme, addition):
        if not self.is_base_rhyme(base_rhyme):
            return base_rhyme

        if base_rhyme == 'uo' and addition == 'w':
            return 'uơ'

        rhyme_len = len(base_rhyme)
        addition_len = len(addition)
        __base_rhyme = list(base_rhyme)
        # base_rhyme = 'uyen' addition = 'e'
        # uu + w -> uu
        # uo + w ->
        # count w
        nums_w = 0

        for i in range(addition_len):
            if addition[i] == u'w':
                nums_w += 1
        cur_index = 0

        # ao, eu
        # awow, ew
        for cur_add in range(addition_len):
            a = addition[cur_add]
            while cur_index < rhyme_len:
                cur_char = __base_rhyme[cur_index]
                if cur_char == a:
                    if a == 'a':
                        __base_rhyme[cur_index] = 'â'
                        break
                    elif a == 'o':
                        __base_rhyme[cur_index] = 'ô'
                        break
                    elif a == 'e':
                        __base_rhyme[cur_index] = 'ê'
                        break
                elif a == 'w':
                    if nums_w == 1:
                        # TH dac biet oa_w, ou_w
                        is_case_oa = cur_char == u'o' and cur_index + 1 < rhyme_len and __base_rhyme[
                                                                                            cur_index + 1] == u'a'
                        if is_case_oa:
                            __base_rhyme[cur_index + 1] = u'ă'
                            break

                        is_case_uo = cur_char == u'u' and cur_index + 1 < rhyme_len and __base_rhyme[
                                                                                            cur_index + 1] == u'o'
                        if is_case_uo:
                            __base_rhyme[cur_index + 1] = u'ơ'
                            break
                            # is_case_ua = cur_char == u'u' and cur_index + 1 < rhyme_len and __base_rhyme[cur_index + 1] == u'a'
                            # if is_case_ua:
                            #     __base_rhyme[cur_index + 1] = u'ă'
                            #     break
                            # if is_case_oa or is_case_uo or is_case_ua:
                            #     output += current_vowel
                            #     index_vowels += 1
                            #     current_vowel = vowels[index_vowels]

                    if cur_char == 'a':
                        __base_rhyme[cur_index] = 'ă'
                        break
                    elif cur_char == 'o':
                        __base_rhyme[cur_index] = 'ơ'
                        break
                    elif cur_char == 'u':
                        __base_rhyme[cur_index] = 'ư'
                        break

                cur_index += 1

        rhyme = ''.join(__base_rhyme)

        if not self.is_rhyme(rhyme):
            return base_rhyme
        return rhyme


class Syllable:
    MAX_SYLLABLE_LEN = 7

    def __init__(self, lang):
        self.lang = lang
        self.syllables = self.find_all_vn_syllables()
        self.syllable_trie = build_trie(self.syllables)
        self.telex_syllables = [self.any_to_telex(s) for s in self.syllables]
        self.telex_syllable_trie = build_trie(self.telex_syllables)

    def is_syllable(self, s):
        s = s.lower()
        syllable_len = len(s)
        if syllable_len == 0 or syllable_len > Syllable.MAX_SYLLABLE_LEN:
            return False
        return self.syllable_trie.is_valid_word(s)

    def is_telex_syllable(self, s):
        s = s.lower()
        syllable_len = len(s)
        if syllable_len == 0:
            return False
        return self.telex_syllable_trie.is_valid_word(s)

    def to_consonant_rhyme_tone(self, vn_syllable):
        if not self.is_syllable(vn_syllable):
            return None, None, None

        start_rhyme_index = 0
        for i, c in enumerate(vn_syllable):
            if self.lang.char.is_vowel(c):
                start_rhyme_index = i
                break

        consonant = vn_syllable[0:start_rhyme_index]
        rhyme = vn_syllable[start_rhyme_index:]
        if consonant == u'g':
            if len(rhyme) >= 2:
                if rhyme[0] == 'i':
                    if self.lang.char.is_vowel(rhyme[1]):
                        rhyme = rhyme[1:]
                        consonant += 'i'
                    else:
                        consonant += 'i'

        elif consonant == u'q':
            if len(rhyme) >= 2:
                if rhyme[0] == u'u' and rhyme[1] in Char.TONES_VOWELS['a'] + Char.TONES_VOWELS['ă'] + Char.TONES_VOWELS[
                    'e']:
                    rhyme = list(rhyme)
                    rhyme[0] = u'o'
                    rhyme = ''.join(rhyme)

        rhyme, tone = self.lang.rhyme.to_rhyme_tone(rhyme)

        return consonant, rhyme, tone

    def add_consonant_rhyme_tone(self, consonant, rhyme, tone):
        if not (self.lang.consonant.is_consonant(consonant) or consonant == '') or not self.lang.rhyme.is_rhyme(
                rhyme) or not self.lang.tone.is_tone(tone):
            return None

        is_q = consonant == 'q'
        is_gi = consonant == 'gi'

        if is_gi:
            if rhyme[0] == 'i':
                consonant = 'g'

        # tone rhyme
        rhyme = self.lang.rhyme.add_rhyme_tone(rhyme, tone, is_q)

        if is_q:
            rhyme = list(rhyme)
            if rhyme[0] == 'o':
                rhyme[0] = 'u'
            elif rhyme[0] != 'u':
                consonant = 'qu'
            rhyme = ''.join(rhyme)
        return consonant + rhyme

    def to_consonant_base_rhyme_addition_tone(self, syllable):
        if not self.is_syllable(syllable):
            return None, None, None, None
        c, r, t = self.to_consonant_rhyme_tone(syllable)
        br, a = self.lang.rhyme.to_base_rhyme_addition(r)

        if c == 'đ':
            c = 'd'
            a = 'd' + a

        t = Tone.TONES[t]
        return c, br, a, t

    def add_consonant_base_rhyme_addition_tone(self, c, br, a, t):
        # print(self.lang.consonant.is_consonant(c))
        # print(self.lang.rhyme.is_base_rhyme(br))
        # print(self.lang.tone.is_addition(a))
        # print(self.lang.tone.is_tone(t))
        if not (self.lang.consonant.is_consonant(c) or c == '') or not self.lang.rhyme.is_base_rhyme(
                br) or not self.lang.tone.is_addition(a) or not self.lang.tone.is_tone(t):
            return None

        if c != '' and c == 'd':
            if a != '' and a[0] == 'd':
                c = 'đ'
                a = a[1:]

        r = self.lang.rhyme.add_base_rhyme_addition(br, a)
        return self.add_consonant_rhyme_tone(c, r, t)

    def vn_to_telex(self, vn_syllable):
        if not self.is_syllable(vn_syllable):
            return vn_syllable

        c, r, t = self.to_consonant_rhyme_tone(vn_syllable)

        base_r, a = self.lang.rhyme.to_base_rhyme_addition(r)

        if c == 'gi' and base_r[0] == 'i':
            c = 'g'
        elif c == 'đ':
            c = 'd'
            a = 'd' + a
        elif c == 'q':
            if len(base_r) >= 2:
                base_r = list(base_r)
                if base_r[0] == 'o' and base_r[1] in 'ae':
                    base_r[0] = 'u'
                base_r = ''.join(base_r)

        return c + base_r + a + Tone.TONES[t]

    def telex_to_consonant_rhyme_tone(self, syllable):
        syllable_len = len(syllable)
        if syllable_len == 0:
            return None, None, None

        # Find tone
        last_char = syllable[-1]
        tone = 0
        for i, t in enumerate(Tone.TONES):
            if last_char == t:
                tone = i
        if tone > 0:
            syllable = syllable[:syllable_len - 1]
        syllable_len = len(syllable)
        addition_start_index = syllable_len - 1
        current_index = 0

        # Duyet nguoc tu duoi len tim vi tri bat dau cua addition
        for i in reversed(range(syllable_len)):
            if syllable[i] == u'd':
                addition_start_index = i
                break
            elif syllable[i] == u'w':
                pass
            elif syllable[i] in u'aeo':
                is_done = False
                while True:
                    if current_index == i:
                        addition_start_index = i + 1
                        is_done = True
                        break
                    if syllable[current_index] == syllable[i]:
                        # ki tu vi tri i thuoc addition
                        if current_index == i - 1:
                            addition_start_index = i
                            is_done = True
                        break
                    else:
                        current_index += 1
                # Hoan thanh break vong for
                if is_done:
                    break
            else:
                addition_start_index = i + 1
                break

        addition = ''
        if addition_start_index == 0:
            base = syllable
        else:
            base = syllable[0:addition_start_index]
            addition = syllable[addition_start_index:]

        if len(addition) > 0 and addition[0] == 'd':
            addition = addition[1:]
            base = list(base)
            base[0] = u'đ'
            base = ''.join(base)

        start_base_rhyme_index = 0

        base_len = len(base)
        for i in range(base_len):
            if base[i] in 'aeiouy':
                start_base_rhyme_index = i
                break
        consonant = base[:start_base_rhyme_index]
        base_rhyme = base[start_base_rhyme_index:]

        if consonant == u'g':
            if len(base_rhyme) >= 2:
                if base_rhyme[0] == u'i':
                    if base_rhyme[1] in 'aeiouy':
                        base_rhyme = base_rhyme[1:]
                        consonant += 'i'
                    else:
                        consonant += 'i'

        if consonant == u'q':
            if len(base_rhyme) >= 2:
                if base_rhyme[0] == u'u' and base_rhyme[1] in u'ae':
                    base_rhyme = list(base_rhyme)
                    base_rhyme[0] = u'o'
                    base_rhyme = ''.join(base_rhyme)

        # Truong hop 'o' o cuoi
        if len(addition) > 0 and addition[0] == u'o':
            if self.lang.rhyme.is_rhyme(base_rhyme + 'o'):
                base_rhyme += 'o'
                addition = addition[1:]

        rhyme = self.lang.rhyme.add_base_rhyme_addition(base_rhyme, addition)

        # giữ gìn -> gi ư x, gi in f
        return consonant, rhyme, tone

    def telex_to_vn(self, telex_syllable):
        c, r, t = self.telex_to_consonant_rhyme_tone(telex_syllable)
        return self.add_consonant_rhyme_tone(c, r, t)

    def any_to_telex(self, word):
        output = ''
        addition = ''
        tone = ''
        for c in word:
            if not self.lang.char.is_vowel(c):
                if c == 'đ':
                    output += 'd'
                    addition = 'd' + addition
                else:
                    output += c
            else:
                c, t = self.lang.char.to_vowel_tone(c)
                if tone == '':
                    tone = Tone.TONES[t]
                for i, v in enumerate(Char.VOWELS):
                    if v == c:
                        output += Char.BASE_VOWELS[i]
                        addition += Char.ADDITION_VOWELS[i]
                        break

        output += addition + tone
        return output

    def find_all_vn_syllables(self):
        syllables = []
        for rhyme in sorted(Rhyme.RULE_RHYMES.keys()):
            rule = Rhyme.RULE_RHYMES[rhyme]
            is_full_tone = rule[-1] == '1'
            if is_full_tone:
                tones = [0, 1, 2, 3, 4, 5]  # full
            else:
                tones = [2, 5]  # sac, nang

            consonants = []
            consonants.append('')
            consonants.extend(Consonant.CONSONANTS)

            if rule[0] == '0':
                consonants.remove('c')
            if rule[1] == '0':
                consonants.remove('k')
            if rule[2] == '0':
                consonants.remove('q')
            if rule[3] == '0':
                consonants.remove('g')
            if rule[4] == '0':
                consonants.remove('gh')
            if rule[5] == '0':
                consonants.remove('ng')
            if rule[6] == '0':
                consonants.remove('ngh')

            for c in consonants:
                for t in tones:
                    syllable = self.add_consonant_rhyme_tone(c, rhyme, t)
                    syllables.append(syllable)
        syllables = sorted(syllables)
        # with open('save/syllables.txt', 'w', encoding='utf8') as f:
        #     for s in syllables:
        #         f.write(s + '\n')

        return syllables


class VnLang:
    END_PUNCTUATIONS = [
        '.', '?', '!'
    ]
    SPLIT_PUNCTUATIONS = [
        ':', ',', ';'
    ]
    PUNCTUATIONS = [
        '%', ':', '-', '/', ',', ';', '.', '?', '!', '(', ')'
    ]

    def __init__(self):
        self.name = 'vn'
        self.tone = Tone(self)
        self.char = Char(self)
        self.consonant = Consonant(self)
        self.rhyme = Rhyme(self)
        self.syllable = Syllable(self)

    def remove_diacritics(self, text):
        output = ''
        for c in text:
            if self.char.is_vowel(c):
                output += self.char.to_base_vowel(c)
            elif c == 'đ':
                output += 'd'
            else:
                output += c

        return output

    def any_to_telex(self, text):
        new_text = ''
        for c in text:
            if self.char.is_char_or_number(c) or c == ' ':
                new_text += c
        if new_text[-1] == ' ':
            new_text = new_text[0:-1]

        words = new_text.lower().split(' ')
        words_len = len(words)
        new_text = ''
        for i, w in enumerate(words):
            new_text += self.syllable.any_to_telex(w)
            if i < words_len - 1:
                new_text += ' '
        return new_text


if __name__ == '__main__':
    start = time.time()
    vn_lang = VnLang()
    print(time.time() - start)
    print(vn_lang.name)
    print(vn_lang.char.NUMBERS)
    print(vn_lang.char.BASE_CHARACTERS)
    print(vn_lang.char.FULL_CHARACTERS)

    print(vn_lang.char.is_vowel('a'))
    print(vn_lang.char.is_vowel('b'))
    print(vn_lang.char.to_base_vowel('â'))
    print(vn_lang.char.to_base_vowel('b'))
    print(vn_lang.char.add_a_vowel_tone('a', Tone.SAC))
    print(vn_lang.char.add_a_vowel_tone('a', Tone.NGANG))
    print(vn_lang.char.add_a_vowel_tone('a', Tone.HUYEN))
    print(vn_lang.char.add_a_vowel_tone('a', Tone.NGA))
    print(vn_lang.char.add_a_vowel_tone('a', Tone.NANG))
    print(vn_lang.char.add_a_vowel_tone('a', -1))
    print(vn_lang.char.add_a_vowel_tone('b', Tone.NANG))

    print(vn_lang.consonant.is_consonant('ngh'))
    print(vn_lang.consonant.is_consonant('a'))

    print(vn_lang.rhyme.is_rhyme('ai'))
    print(vn_lang.rhyme.is_rhyme('m'))
    print(vn_lang.rhyme.to_rhyme_tone('hải'))
    print(vn_lang.rhyme.to_rhyme_tone('ải'))
    print(vn_lang.rhyme.to_rhyme_tone('ướt'))
    print(vn_lang.rhyme.to_rhyme_tone('ưỚt'))
    print(vn_lang.rhyme.to_rhyme_tone('ÒA'))
    print(vn_lang.rhyme.to_rhyme_tone('binh'))

    print(vn_lang.rhyme.add_rhyme_tone('OA', Tone.SAC))
    print(vn_lang.rhyme.add_rhyme_tone('Oan', Tone.SAC))
    print(vn_lang.rhyme.add_rhyme_tone('hOan', Tone.SAC))
    print(vn_lang.rhyme.add_rhyme_tone('oeo', Tone.SAC))

    print(vn_lang.syllable.is_syllable('hai'))
    print(vn_lang.syllable.to_consonant_rhyme_tone('hai'))
    print(vn_lang.syllable.to_consonant_rhyme_tone('cuộc'))
    print(vn_lang.syllable.to_consonant_rhyme_tone('quos'))

    print(vn_lang.syllable.to_consonant_base_rhyme_addition_tone('cuộc'))
    # print(vn_lang.syllable.find_all_vn_syllables())
    print(vn_lang.syllable.to_consonant_base_rhyme_addition_tone('quos'))
    print(vn_lang.syllable.to_consonant_base_rhyme_addition_tone('ước'))
    print(vn_lang.syllable.add_consonant_rhyme_tone('c', 'on', Tone.NGANG))
    print(vn_lang.syllable.add_consonant_rhyme_tone('c', 'ông', Tone.SAC))
    print(vn_lang.syllable.add_consonant_base_rhyme_addition_tone('c', 'ong', 'o', Tone.NGA))
    print(vn_lang.syllable.add_consonant_base_rhyme_addition_tone('', 'ong', 'o', Tone.SAC))
    print(vn_lang.syllable.add_consonant_base_rhyme_addition_tone('d', 'ong', 'd', Tone.SAC))

    # source_dict = CharDictionary(has_diacritics=False)
    # target_dict = CharDictionary(has_diacritics=True)
