class TrieNode:
    def __init__(self):
        self.word = None
        self.children = {}

    def insert(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                node.children[letter] = TrieNode()
            node = node.children[letter]
        node.word = word

    def is_valid_word(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                return False
            else:
                node = node.children[letter]
        if node.word == word:
            return True
        return False


def build_trie(words):
    trie = TrieNode()
    for w in words:
        trie.insert(w)
    return trie


def build_dictionary(values):
    key2value = sorted(set(values))
    value2key = dict((v, i) for i, v in enumerate(key2value))
    key2value = list(value2key)
    return key2value, value2key
