NUMBER = [u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9']
BASE_CHARACTER = [
    u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
    u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z'
]

CASE_SENSITIVE_BASE_CHARACTER = BASE_CHARACTER + [c.upper() for c in BASE_CHARACTER]

MORES = [
    u' ', u'!', u',', u'-', u'.', u'...', u';', u'?', u'/', u'\x00', u'<OOV>'
]

CHAR_MORES = [
    ' ', u'\x00', '<url>', '<email>', '<date>', '<name>', '<location>',
    '<number>', '<percentnumber>', '<money>',
    '<oov>', '<sos>', '<eos>'
]
SUB_WORD_MORES = [
    u' ', u'\x00', u'<OOV>'
]

PUNCTUATIONS = [
    ':', '-', '/', ',', ';', '.', '?', '!'
]

FULL_CHARACTER = [
    u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
    u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z',
    u'à', u'á', u'â', u'ã', u'è', u'é', u'ê', u'ì', u'í', u'ò', u'ó',
    u'ô', u'õ', u'ù', u'ú', u'ý', u'ă', u'đ', u'ĩ', u'ũ', u'ơ', u'ư',
    u'ạ', u'ả', u'ấ', u'ầ', u'ẩ', u'ẫ', u'ậ', u'ắ', u'ằ', u'ẳ', u'ẵ', u'ặ',
    u'ẹ', u'ẻ', u'ẽ', u'ế', u'ề', u'ể', u'ễ', u'ệ', u'ỉ', u'ị', u'ọ', u'ỏ',
    u'ố', u'ồ', u'ổ', u'ỗ', u'ộ', u'ớ', u'ờ', u'ở', u'ỡ', u'ợ', u'ụ',
    u'ủ', u'ứ', u'ừ', u'ử', u'ữ', u'ự', u'ỳ', u'ỵ', u'ỷ', u'ỹ'
]

CASE_SENSITIVE_FULL_CHARACTER = FULL_CHARACTER + [c.upper() for c in FULL_CHARACTER]

SINGLE_CONSONANTS = [u'b', u'c', u'd', u'đ', u'g', u'h', u'k', u'l', u'm', u'n', u'p', u'q', u'r', u's', u't', u'v',
                     u'x']
# bo qu
DI_CONSONANTS = [u'ch', u'gh', u'gi', u'kh', u'ng', u'nh', u'ph', u'th', u'tr']
TRI_CONSONANTS = [u'ngh']
CONSONANTS = SINGLE_CONSONANTS + DI_CONSONANTS + TRI_CONSONANTS
# d r gi
# c k q  g gh ng ngh du_dau vi_tri_them_dau(chi so cua nguyen am)
# 0 0 0  0 0  0  0   0
RULE_RHYMES = {
    # a
    u'a': '10010101',
    u'ai': '10010101',
    u'am': '10010101',
    u'an': '10010101',
    u'ang': '10010101',
    u'anh': '10010101',
    u'ao': '10010101',
    u'au': '10010101',
    u'ay': '10010101',
    u'ac': '10010100',
    u'ach': '10010100',
    u'ap': '10010100',
    u'at': '10010100',
    # # ă
    u'ăm': '10010101',
    u'ăn': '10010101',
    u'ăng': '10010101',
    u'ăc': '10010100',
    u'ăp': '10010100',
    u'ăt': '10010100',
    # â
    u'âm': '10010101',
    u'ân': '10010101',
    u'âng': '10010101',
    u'âu': '10010101',
    u'ây': '10010101',
    u'âc': '10010100',
    u'âp': '10010100',
    u'ât': '10010100',
    # e
    u'e': '01001011',
    u'em': '01001011',
    u'en': '01001011',
    u'eng': '01001011',
    u'eo': '01001011',
    u'ec': '01001010',
    u'ep': '01001010',
    u'et': '01001010',

    # ê
    u'ê': '01001011',
    u'êm': '01001011',
    u'ên': '01001011',
    u'ênh': '01001011',
    u'êu': '01001011',
    u'êch': '01001010',
    u'êp': '01001010',
    u'êt': '01001010',
    # i
    u'i': '01001011',
    u'ia': '01001011',
    u'im': '01001011',
    u'in': '01001011',
    u'inh': '01001011',
    u'iu': '01001011',
    # u'ic': '01001010', ?????????????????
    u'ich': '01001010',
    u'ip': '01001010',
    u'it': '01001010',
    # iê
    u'iêm': '01001011',
    u'iên': '01001011',
    u'iêng': '01001011',
    u'iêu': '01001011',
    u'iêc': '01001010',
    u'iêp': '01001010',
    u'iêt': '01001010',
    # o
    u'o': '10010101',
    # u'oa': '10010101',  # truong hop dac biet
    u'oi': '10010101',
    u'om': '10010101',
    u'on': '10010101',
    u'ong': '10010101',
    u'oc': '10010100',
    u'op': '10010100',
    u'ot': '10010100',
    # oa
    u'oa': '00110101',
    u'oai': '00110101',
    u'oan': '00110101',
    u'oam': '00110101',
    u'oang': '00110101',
    u'oanh': '00110101',
    u'oao': '00110101',
    u'oau': '00110101',
    u'oay': '00110101',
    u'oac': '00110100',
    u'oach': '00110100',
    u'oap': '00110100',
    u'oat': '00110100',
    # oă
    u'oăm': '00110101',
    u'oăn': '00110101',
    u'oăng': '00110101',
    u'oăc': '00110100',
    u'oăt': '00110100',
    u'oăp': '00110100',
    # oe
    u'oe': '00110101',
    u'oem': '00110101',
    u'oen': '00110101',
    # u'oeng': '00110101', ??????????????
    u'oeo': '00110101',
    u'oet': '00110100',
    # oo
    u'oong': '10010101',
    u'ooc': '10010100',
    # ô
    u'ô': '10010101',
    u'ôi': '10010101',
    u'ôm': '10010101',
    u'ôn': '10010101',
    u'ông': '10010101',
    u'ôc': '10010100',
    u'ôp': '10010100',
    u'ôt': '10010100',
    # ơ
    u'ơ': '10010101',
    u'ơi': '10010101',
    u'ơm': '10010101',
    u'ơn': '10010101',
    # u'ơng': '10010101', ?????????????????
    # u'ơc': '10010100', ??????????????????
    u'ơp': '10010100',
    u'ơt': '10010100',
    # u
    u'u': '10010101',
    u'ua': '10010101',
    u'ui': '10010101',
    u'um': '10010101',
    u'un': '10010101',
    u'ung': '10010101',
    u'uy': '10010101',
    u'uc': '10010100',
    u'up': '10010100',
    u'ut': '10010100',
    # uâ
    u'uân': '00110101',
    u'uâng': '00110101',
    u'uây': '00110101',
    u'uât': '00110100',
    # uê
    u'uê': '00110101',
    u'uêu': '00110101',
    u'uênh': '00110101',
    u'uên': '00110101',
    u'uêt': '00110100',
    u'uêch': '00110100',
    # uô
    u'uôi': '10010101',
    u'uôm': '10010101',
    u'uôn': '10010101',
    u'uông': '10010101',
    u'uôc': '10110100',
    u'uôp': '10010100',
    u'uôt': '10010100',
    # uơ
    u'uơ': '00110101',
    # uy
    u'uy': '00110101',
    u'uya': '00110101',
    u'uyn': '00110101',
    u'uynh': '00110101',
    u'uyu': '00110101',
    u'uych': '00110100',
    u'uyt': '00110100',
    # uyê
    u'uyên': '00110101',
    u'uyêt': '00110100',
    # ư
    u'ư': '10010101',
    u'ưa': '10010101',
    u'ưi': '10010101',
    u'ưm': '10010101',
    u'ưn': '10010101',
    u'ưng': '10010101',
    u'ưu': '10010101',
    u'ưc': '10010100',
    # u'ưp': '10010100', ?????????????
    u'ưt': '10010100',
    # ươ
    u'ươi': '10010101',
    u'ươm': '10010101',
    u'ươn': '10010101',
    u'ương': '10010101',
    u'ươu': '10010101',
    u'ươc': '10010100',
    u'ươp': '10010100',
    u'ươt': '10010100',
    # y
    u'y': '01000001',
    # yê
    u'yên': '00000001',
    u'yêm': '00000001',
    u'yêng': '00000001',
    u'yêu': '00000001',
    u'yêt': '00000000',
}

BASE_RHYMES = [
    'a', 'ac', 'ach', 'ai', 'am', 'an', 'ang', 'anh', 'ao', 'ap', 'at', 'au', 'ay',
    'e', 'ec', 'ech', 'em', 'en', 'eng', 'enh', 'eo', 'ep', 'et', 'eu',
    'i', 'ia', 'ich', 'iec', 'iem', 'ien', 'ieng', 'iep', 'iet', 'ieu', 'im', 'in', 'inh', 'ip', 'it', 'iu',
    'o', 'oa', 'oac', 'oach', 'oai', 'oam', 'oan', 'oang', 'oanh', 'oao', 'oap', 'oat', 'oau', 'oay', 'oc', 'oe', 'oem',
    'oen', 'oeo', 'oet', 'oi', 'om', 'on', 'ong', 'ooc', 'oong', 'op', 'ot',
    'u', 'ua', 'uan', 'uang', 'uat', 'uay', 'uc', 'ue', 'uech', 'uen', 'uenh', 'uet', 'ueu', 'ui', 'um', 'un', 'ung',
    'uo', 'uoc', 'uoi', 'uom', 'uon', 'uong', 'uop', 'uot',
    'uou', 'up', 'ut', 'uu', 'uy', 'uya', 'uych', 'uyen', 'uyet', 'uyn', 'uynh', 'uyt', 'uyu',
    'y', 'yem', 'yen', 'yeng', 'yet', 'yeu'
]

ADDITION_TONES = [
    'a', 'af', 'aj', 'ar', 'as', 'ax', 'd', 'da', 'daf', 'daj', 'dar', 'das', 'dax', 'de', 'def', 'dej',
    'der', 'des', 'dex', 'df', 'dj', 'do', 'dof', 'doj', 'dor', 'dos', 'dox', 'dr', 'ds', 'dw', 'dwf', 'dwj', 'dwr',
    'dws', 'dww', 'dwwf', 'dwwj', 'dwwr', 'dwws', 'dwwx', 'dwx', 'dx', 'e', 'ef', 'ej', 'er', 'es', 'ex', 'f', 'j', 'o',
    'of',
    'oj', 'or', 'os', 'ox', 'r', 's', 'w', 'wf', 'wj', 'wr', 'ws', 'ww', 'wwf', 'wwj', 'wwr', 'wws', 'wwx', 'wx', 'x'
]

VOWELS = [u'a', u'ă', u'â', u'e', u'ê', u'i', u'o', u'ô', u'ơ', u'u', u'ư', u'y']
BASE_VOWELS = [u'a', u'a', u'a', u'e', u'e', u'i', u'o', u'o', u'o', 'u', 'u', 'y']
ADDITION_VOWELS = [u'', u'w', u'a', u'', u'e', u'', u'', u'o', u'w', u'', u'w', u'']
TONES = [u'', u'f', u's', u'r', u'x', u'j']
TONE2INDEX = {
    u'': 0, u'f': 1, u's': 2, u'r': 3, u'x': 4, u'j': 5
}
TONES_VOWELS = {
    'a': u'aàáảãạ',
    'ă': u'ăằắẳẵặ',
    'â': u'âầấẩẫậ',
    'e': u'eèéẻẽẹ',
    'ê': u'êềếểễệ',
    'i': u'iìíỉĩị',
    'o': u'oòóỏõọ',
    'ô': u'ôồốổỗộ',
    'ơ': u'ơờớởỡợ',
    'u': u'uùúủũụ',
    'ư': u'ưừứửữự',
    'y': u'yỳýỷỹỵ'
}

ALL_VOWELS = ['a', 'à', 'á', 'ả', 'ã', 'ạ',
              'e', 'è', 'é', 'ẻ', 'ẽ', 'ẹ',
              'i', 'ì', 'í', 'ỉ', 'ĩ', 'ị',
              'o', 'ò', 'ó', 'ỏ', 'õ', 'ọ',
              'u', 'ù', 'ú', 'ủ', 'ũ', 'ụ',
              'y', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ',
              'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ',
              'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ',
              'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ',
              'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ',
              'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ',
              'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'
              ]


class TrieNode:
    def __init__(self):
        self.word = None
        self.children = {}

    def insert(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                node.children[letter] = TrieNode()
            node = node.children[letter]
        node.word = word

    def is_valid_word(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                return False
            else:
                node = node.children[letter]
        if node.word == word:
            return True
        return False


def build_trie(words):
    trie = TrieNode()
    for w in words:
        trie.insert(w)
    return trie


def build_dictionary(words):
    index2word = sorted(set(words))
    word2index = dict((v, i) for i, v in enumerate(index2word))
    index2word = list(index2word)
    return index2word, word2index


#
# index2base_char, base_char2index = build_dictionary(BASE_CHARACTER + NUMBER + MORES)
# index2full_char, full_char2index = build_dictionary(FULL_CHARACTER + NUMBER + MORES)
# case_sensitive_index2base_char, case_sensitive_base_char2index = build_dictionary(
#     CASE_SENSITIVE_BASE_CHARACTER + NUMBER + MORES)
# case_sensitive_index2full_char, case_sensitive_full_char2index = build_dictionary(
#     CASE_SENSITIVE_FULL_CHARACTER + NUMBER + MORES)

index2base_char, base_char2index = build_dictionary(BASE_CHARACTER + CHAR_MORES + PUNCTUATIONS)
index2full_char, full_char2index = build_dictionary(FULL_CHARACTER + CHAR_MORES + PUNCTUATIONS)

case_sensitive_index2base_char, case_sensitive_base_char2index = build_dictionary(
    CASE_SENSITIVE_BASE_CHARACTER + CHAR_MORES + PUNCTUATIONS)
case_sensitive_index2full_char, case_sensitive_full_char2index = build_dictionary(
    CASE_SENSITIVE_FULL_CHARACTER + CHAR_MORES + PUNCTUATIONS)

index2base_sub_word, base_sub_word2index = build_dictionary(BASE_RHYMES + CONSONANTS + SUB_WORD_MORES)
index2addition_tone, addition_tone2index = build_dictionary(
    ADDITION_TONES + [u'\x00', u'<C_NONE>', u'<AT_NONE>', u'<WS_NONE>'])

plain_char_map = {}
for k, vowels in TONES_VOWELS.items():
    for v in vowels:
        plain_char_map[v] = k


def remove_accent(sequence):
    output = ''
    for c in sequence:
        if is_vowel(c):
            output += to_base_vowel(c)
        elif c == 'đ':
            output += 'd'
        else:
            output += c
    return output


def is_vowel(vowel):
    for v in ALL_VOWELS:
        if v == vowel:
            return True
    return False


def to_base_vowel(vowel):
    vowel, t = to_vowel_tone(vowel)
    for i, v in enumerate(VOWELS):
        if v == vowel:
            return BASE_VOWELS[i]


rhyme_trie = build_trie(RULE_RHYMES.keys())


def is_rhyme(rhyme):
    # for r in RULE_RHYMES.keys():
    #     if r == rhyme:
    #         return True
    # return False
    return rhyme_trie.is_valid_word(rhyme)


def to_vowel_tone(vowel):
    for k in TONES_VOWELS.keys():
        vowels = TONES_VOWELS[k]
        for i in range(len(vowels)):
            if vowel == vowels[i]:
                return k, i
    return vowel, -1


def add_a_vowel_tone(vowel, tone):
    uppercase = vowel.isupper()
    vowel = vowel.lower()
    if vowel == '' or tone == 0:
        return vowel
    vowel = TONES_VOWELS[vowel][tone]
    if uppercase:
        return vowel.upper()
    return vowel


# toned rhyme -> rhyme + tone
def to_rhyme_tone(rhyme):
    if rhyme == '' or len(rhyme) >= 5:
        return rhyme, -1
    rhyme = list(rhyme)
    for i, c in enumerate(rhyme):
        v, t = to_vowel_tone(c)
        if t > 0:
            rhyme[i] = v

            return ''.join(rhyme), t
    return ''.join(rhyme), 0


def add_rhyme_tone(rhyme, tone, is_q=False):
    rhyme_len = len(rhyme)
    if rhyme_len == 0 or tone == 0:
        return rhyme

    rhyme = list(rhyme)
    nb_vowel = 0
    for c in rhyme:
        if c.lower() in VOWELS:
            nb_vowel += 1
        else:
            break

    if nb_vowel == 0:
        return ''.join(rhyme)
    if nb_vowel == 1:
        toned_vowel = rhyme[0]
        if is_vowel(toned_vowel):
            rhyme[0] = add_a_vowel_tone(toned_vowel, tone)
            return ''.join(rhyme)

    for i in reversed(range(rhyme_len)):
        c = rhyme[i]
        if c.lower() in u'ăâêôơư':
            rhyme[i] = add_a_vowel_tone(c, tone)
            return ''.join(rhyme)

    if nb_vowel == 2:
        if rhyme[-1] not in VOWELS:
            toned_vowel = rhyme[1]
            rhyme[1] = add_a_vowel_tone(toned_vowel, tone)
            return ''.join(rhyme)

        if is_q:
            toned_vowel = rhyme[1]
            rhyme[1] = add_a_vowel_tone(toned_vowel, tone)
            return ''.join(rhyme)

        toned_vowel = rhyme[0]
        rhyme[0] = add_a_vowel_tone(toned_vowel, tone)
        return ''.join(rhyme)

    if nb_vowel == 3:
        toned_vowel = rhyme[1]
        rhyme[1] = add_a_vowel_tone(toned_vowel, tone)
        return ''.join(rhyme)

    # Phong truong hop sai
    return ''.join(rhyme)


def to_base_rhyme_addition(rhyme):
    addition = u''
    rhyme = list(rhyme)
    vowels_len = len(VOWELS)
    for i in range(len(rhyme)):
        for j in range(vowels_len):
            if rhyme[i] == VOWELS[j]:
                rhyme[i] = BASE_VOWELS[j]
                addition += ADDITION_VOWELS[j]
                break

    return ''.join(rhyme), addition


def add_base_rhyme_addition(base_rhyme, addition):
    if len(base_rhyme) >= 5:
        return base_rhyme
    if base_rhyme == 'uo' and addition == 'w':
        return 'uơ'
    rhyme_len = len(base_rhyme)
    addition_len = len(addition)
    base_rhyme = list(base_rhyme)
    # base_rhyme = 'uyen' addition = 'e'
    # uu + w -> uu
    # uo + w ->
    # count w
    nums_w = 0

    for i in range(addition_len):
        if addition[i] == u'w':
            nums_w += 1
    cur_index = 0

    # ao, eu
    # awow, ew
    for cur_add in range(addition_len):
        a = addition[cur_add]
        while cur_index < rhyme_len:
            cur_char = base_rhyme[cur_index]
            if cur_char == a:
                if a == 'a':
                    base_rhyme[cur_index] = 'â'
                    break
                elif a == 'o':
                    base_rhyme[cur_index] = 'ô'
                    break
                elif a == 'e':
                    base_rhyme[cur_index] = 'ê'
                    break
            elif a == 'w':
                if nums_w == 1:
                    # TH dac biet oa_w, ou_w
                    is_case_oa = cur_char == u'o' and cur_index + 1 < rhyme_len and base_rhyme[cur_index + 1] == u'a'
                    if is_case_oa:
                        base_rhyme[cur_index + 1] = u'ă'
                        break

                    is_case_uo = cur_char == u'u' and cur_index + 1 < rhyme_len and base_rhyme[cur_index + 1] == u'o'
                    if is_case_uo:
                        base_rhyme[cur_index + 1] = u'ơ'
                        break
                        # is_case_ua = cur_char == u'u' and cur_index + 1 < rhyme_len and base_rhyme[cur_index + 1] == u'a'
                        # if is_case_ua:
                        #     base_rhyme[cur_index + 1] = u'ă'
                        #     break
                        # if is_case_oa or is_case_uo or is_case_ua:
                        #     output += current_vowel
                        #     index_vowels += 1
                        #     current_vowel = vowels[index_vowels]

                if cur_char == 'a':
                    base_rhyme[cur_index] = 'ă'
                    break
                elif cur_char == 'o':
                    base_rhyme[cur_index] = 'ơ'
                    break
                elif cur_char == 'u':
                    base_rhyme[cur_index] = 'ư'
                    break

            cur_index += 1

    return ''.join(base_rhyme)


def telex_to_vn(telex_syllable):
    c, r, t = telex_to_consonant_rhyme_tone(telex_syllable)
    return add_consonant_rhyme_tone(c, r, t)


def vn_to_telex(vn_syllable):
    c, r, t = to_consonant_rhyme_tone(vn_syllable)

    base_r, a = to_base_rhyme_addition(r)

    if c == 'gi' and base_r[0] == 'i':
        c = 'g'
    elif c == 'đ':
        c = 'd'
        a = 'd' + a
    elif c == 'q':
        if len(base_r) >= 2:
            base_r = list(base_r)
            if base_r[0] == 'o' and base_r[1] in 'ae':
                base_r[0] = 'u'
            base_r = ''.join(base_r)

    return c + base_r + a + TONES[t]


def telex_to_consonant_rhyme_tone(syllable):
    syllable_len = len(syllable)
    if syllable_len == 0:
        return '', '', ''
    # Find tone
    last_char = syllable[-1]
    tone = 0
    for i, t in enumerate(TONES):
        if last_char == t:
            tone = i
    if tone > 0:
        syllable = syllable[:syllable_len - 1]
    syllable_len = len(syllable)
    addition_start_index = syllable_len - 1
    current_index = 0

    # Duyet nguoc tu duoi len tim vi tri bat dau cua addition
    for i in reversed(range(syllable_len)):
        if syllable[i] == u'd':
            addition_start_index = i
            break
        elif syllable[i] == u'w':
            pass
        elif syllable[i] in u'aeo':
            is_done = False
            while True:
                if current_index == i:
                    addition_start_index = i + 1
                    is_done = True
                    break
                if syllable[current_index] == syllable[i]:
                    # ki tu vi tri i thuoc addition
                    if current_index == i - 1:
                        addition_start_index = i
                        is_done = True
                    break
                else:
                    current_index += 1
            # Hoan thanh break vong for
            if is_done:
                break
        else:
            addition_start_index = i + 1
            break

    addition = ''
    if addition_start_index == 0:
        base = syllable
    else:
        base = syllable[0:addition_start_index]
        addition = syllable[addition_start_index:]

    if len(addition) > 0 and addition[0] == 'd':
        addition = addition[1:]
        base = list(base)
        base[0] = u'đ'
        base = ''.join(base)

    start_base_rhyme_index = 0

    base_len = len(base)
    for i in range(base_len):
        if base[i] in 'aeiouy':
            start_base_rhyme_index = i
            break
    consonant = base[:start_base_rhyme_index]
    base_rhyme = base[start_base_rhyme_index:]

    if consonant == u'g':
        if len(base_rhyme) >= 2:
            if base_rhyme[0] == u'i':
                if base_rhyme[1] in 'aeiouy':
                    base_rhyme = base_rhyme[1:]
                    consonant += 'i'
                else:
                    consonant += 'i'

    if consonant == u'q':
        if len(base_rhyme) >= 2:
            if base_rhyme[0] == u'u' and base_rhyme[1] in u'ae':
                base_rhyme = list(base_rhyme)
                base_rhyme[0] = u'o'
                base_rhyme = ''.join(base_rhyme)

    # Truong hop 'o' o cuoi
    if len(addition) > 0 and addition[0] == u'o':
        if is_rhyme(base_rhyme + 'o'):
            base_rhyme += 'o'
            addition = addition[1:]

    rhyme = add_base_rhyme_addition(base_rhyme, addition)

    # giữ gìn -> gi ư x, gi in f
    return consonant, rhyme, tone


def to_consonant_rhyme_tone(vn_syllable):
    syllable_len = len(vn_syllable)
    if syllable_len == 0 or syllable_len > 7:
        return None

    start_rhyme_index = 0
    for i, c in enumerate(vn_syllable):
        if is_vowel(c):
            start_rhyme_index = i
            break

    consonant = vn_syllable[0:start_rhyme_index]
    rhyme = vn_syllable[start_rhyme_index:]
    if consonant == u'g':
        if len(rhyme) >= 2:
            if rhyme[0] == 'i':
                if is_vowel(rhyme[1]):
                    rhyme = rhyme[1:]
                    consonant += 'i'
                else:
                    consonant += 'i'

    elif consonant == u'q':
        if len(rhyme) >= 2:
            if rhyme[0] == u'u' and rhyme[1] in TONES_VOWELS['a'] + TONES_VOWELS['ă'] + TONES_VOWELS['e']:
                rhyme = list(rhyme)
                rhyme[0] = u'o'
                rhyme = ''.join(rhyme)

    rhyme, tone = to_rhyme_tone(rhyme)

    return consonant, rhyme, tone


def add_consonant_rhyme_tone(consonant, rhyme, tone):
    is_q = consonant == 'q'
    is_gi = consonant == 'gi'

    if is_gi:
        if rhyme[0] == 'i':
            consonant = 'g'

    # tone rhyme
    rhyme = add_rhyme_tone(rhyme, tone, is_q)

    if is_q:
        rhyme = list(rhyme)
        if rhyme[0] == 'o':
            rhyme[0] = 'u'
        elif rhyme[0] != 'u':
            consonant = 'qu'
        rhyme = ''.join(rhyme)
    return consonant + rhyme


def vn_to_consonant_base_rhyme_addition_tone(syllable):
    if not is_syllable(syllable):
        return None
    c, r, t = to_consonant_rhyme_tone(syllable)
    br, a = to_base_rhyme_addition(r)

    if c == 'đ':
        c = 'd'
        a = 'd' + a

    t = TONES[t]
    return c, br, a, t
    # tmp = []
    # if c != '':
    #     tmp.append(c)
    # if br != '':
    #     tmp.append(br)
    # if a != '':
    #     if a[0] == 'd':
    #         tmp.append(a[0])
    #         if len(a) > 1:
    #             tmp.append(a[1:])
    #     else:
    #         tmp.append(a)
    # if t != '':
    #     tmp.append(t)
    # return tmp


def add_consonant_base_rhyme_addition_tone(c, br, a, t):
    if c != '' and c == 'd':
        if a != '' and a[0] == 'd':
            c = 'đ'
            a = a[1:]

    r = add_base_rhyme_addition(br, a)
    return add_consonant_rhyme_tone(c, r, t)


def find_all_vn_syllables():
    syllables = []
    for rhyme in sorted(RULE_RHYMES.keys()):
        rule = RULE_RHYMES[rhyme]
        is_full_tone = rule[-1] == '1'
        if is_full_tone:
            tones = [0, 1, 2, 3, 4, 5]  # full
        else:
            tones = [2, 5]  # sac, nang

        consonants = []
        consonants.append('')
        consonants.extend(CONSONANTS)

        if rule[0] == '0':
            consonants.remove('c')
        if rule[1] == '0':
            consonants.remove('k')
        if rule[2] == '0':
            consonants.remove('q')
        if rule[3] == '0':
            consonants.remove('g')
        if rule[4] == '0':
            consonants.remove('gh')
        if rule[5] == '0':
            consonants.remove('ng')
        if rule[6] == '0':
            consonants.remove('ngh')

        for c in consonants:
            for t in tones:
                syllable = add_consonant_rhyme_tone(c, rhyme, t)

                syllables.append(syllable)
    syllables = sorted(syllables)
    # with open('save/syllables.txt', 'w', encoding='utf8') as f:
    #     for s in syllables:
    #         f.write(s + '\n')

    return syllables


def find_all_base_rhyme_and_addition_tone():
    base_rhymes = []
    additions = []
    for rhyme in sorted(RULE_RHYMES.keys()):
        br, a = to_base_rhyme_addition(rhyme)
        base_rhymes.append(br)
        additions.append(a)

    base_rhymes = list(sorted(set(base_rhymes)))
    additions = list(sorted(set(additions)))
    new_additions = []
    for a in additions:
        for tone in TONES:
            new_additions.append(a + tone)
            new_additions.append('d' + a + tone)

    new_additions = sorted(new_additions)
    print(new_additions)
    print(len(new_additions))
    # print(base_rhymes)
    # print(len(base_rhymes))
    # print(additions)
    # print(len(additions))


def any_to_telex(word):
    output = ''
    addition = ''
    tone = ''
    for c in word:
        if not is_vowel(c):
            if c == 'đ':
                output += 'd'
                addition = 'd' + addition
            else:
                output += c
        else:
            c, t = to_vowel_tone(c)
            if tone == '':
                tone = TONES[t]
            for i, v in enumerate(VOWELS):
                if v == c:
                    output += BASE_VOWELS[i]
                    addition += ADDITION_VOWELS[i]
                    break

    output += addition + tone
    return output


all_syllables = find_all_vn_syllables()
telex_syllales = [any_to_telex(s) for s in all_syllables]
syllables_trie = build_trie(telex_syllales)


# find_all_base_rhyme_and_addition_tone()


def is_syllable(syllable):
    syllable = any_to_telex(syllable)
    return syllables_trie.is_valid_word(syllable)


def test():
    print('a is a vowel ? -->', is_vowel('a'))
    print('u is a vowel ? -->', is_vowel('u'))
    print('i is a vowel ? -->', is_vowel('i'))

    print('b is a vowel ? -->', is_vowel('b'))
    print('ch is a vowel ? -->', is_vowel('ch'))
    print('ô is a vowel ? -->', is_vowel('ô'))
    print('gi is a vowel ? -->', is_vowel('gi'))

    print('ich is a rhyme ? -->', is_rhyme('ich'))
    print('uôn is a rhyme ? -->', is_rhyme('uôn'))
    print('ưc is a rhyme ? -->', is_rhyme('ưc'))
    print('a is a rhyme ? -->', is_rhyme('a'))

    print('á -->', to_vowel_tone(u'á'))
    print('ì -->', to_vowel_tone(u'ì'))
    print('as -->', add_a_vowel_tone('a', 2))
    print('óc -->', to_rhyme_tone(u'óc'))
    print('uyển -->', to_rhyme_tone(u'uyển'))
    print('uôn -->', to_rhyme_tone(u'uôn'))

    print('uôn -->', to_base_rhyme_addition('uôn'))
    print('ươu -->', to_base_rhyme_addition('ươu'))
    print('uyên -->', to_base_rhyme_addition('uyên'))

    print('uyene -->', add_base_rhyme_addition('uyen', 'e'))
    print('uono -->', add_base_rhyme_addition('uon', 'o'))
    print('uongww -->', add_base_rhyme_addition('uong', 'ww'))
    print('uow -->', add_base_rhyme_addition('uo', 'w'))

    # print(telex_to_vn())
    print('bongs --> ', telex_to_consonant_rhyme_tone('bongs'))
    print('mau --> ', telex_to_consonant_rhyme_tone('mau'))
    print('chuyenej --> ', telex_to_consonant_rhyme_tone('chuyenej'))
    print('duongdwwf --> ', telex_to_consonant_rhyme_tone('duongdwwf'))
    print('hongof --> ', telex_to_consonant_rhyme_tone('hongof'))
    print('giar --> ', telex_to_consonant_rhyme_tone('giar'))
    print('gianhf --> ', telex_to_consonant_rhyme_tone('gianhf'))
    print('giacas --> ', telex_to_consonant_rhyme_tone('giacas'))
    print('ginf --> ', telex_to_consonant_rhyme_tone('ginf'))

    print('nguyenex --> ', telex_to_consonant_rhyme_tone('nguyenex'))
    print('quanaf --> ', telex_to_consonant_rhyme_tone('quanaf'))
    print('quar --> ', telex_to_consonant_rhyme_tone('quar'))
    print('quanr --> ', telex_to_consonant_rhyme_tone('quanr'))

    print('quần --> ', to_consonant_rhyme_tone('quần'))
    print('quỳnh --> ', to_consonant_rhyme_tone('quỳnh'))
    print('giấc --> ', to_consonant_rhyme_tone('giấc'))
    print('gìn --> ', to_consonant_rhyme_tone('gìn'))
    print('quan --> ', to_consonant_rhyme_tone('quan'))
    print('huyện --> ', to_consonant_rhyme_tone('huyện'))
    print('bình --> ', to_consonant_rhyme_tone('bình'))

    print('nguyenex --> ', telex_to_vn('nguyenex'))
    print('quanaf --> ', telex_to_vn('quanaf'))
    print('quar --> ', telex_to_vn('quar'))
    print('quanr --> ', telex_to_vn('quanr'))

    print('quần --> ', vn_to_telex('quần'))
    print('quỳnh --> ', vn_to_telex('quỳnh'))
    print('giấc --> ', vn_to_telex('giấc'))
    print('gìn --> ', vn_to_telex('gìn'))
    print('quan --> ', vn_to_telex('quan'))
    print('huyện --> ', vn_to_telex('huyện'))
    print('bình --> ', vn_to_telex('bình'))

    print('quặng  -->', vn_to_telex('quặng'))
    print('quangwj  -->', telex_to_vn('quangwj'))

    print('bóng đá -->', remove_accent('bóng đá'))
    print('cuộc sống -- >', remove_accent('cuộc sống'))
    print('bóng-->', vn_to_consonant_base_rhyme_addition_tone('bóng'))
    print('ông-->', vn_to_consonant_base_rhyme_addition_tone('ông'))
    print('đường-->', vn_to_consonant_base_rhyme_addition_tone('đường'))
    print('quắp-->', vn_to_consonant_base_rhyme_addition_tone('quắp'))
    # all_sub_words = []
    # for s in find_all_vn_syllables():
    #     all_sub_words.extend(vn_to_consonant_base_rhyme_addition_tone(s))
    #     c, br, a, t = vn_to_consonant_base_rhyme_addition_tone(s)
    #     # print(s, '-->', c, br, a, t)
    #     new_s = add_consonant_base_rhyme_addition_tone(c, br, a, TONE2INDEX[t])
    #     if s != new_s:
    #         print(s, '!=', new_s)
    # all_sub_words = set(all_sub_words)
    # print(len(all_sub_words))

    error = []
    for s in find_all_vn_syllables():
        encode = any_to_telex(s)
        decode = telex_to_vn(encode)

        if decode != s:
            error.append((s, encode, decode))
            print(s, encode, decode)
    print(len(error))

# find_all_vn_syllables()

# 147 sub_word
# test()
