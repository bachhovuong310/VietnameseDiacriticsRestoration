from keras.callbacks import Callback
from collections import defaultdict
import matplotlib.pyplot as plt
import os
import pickle


# History
class EverHistory(Callback):
    def __init__(self, acc_type, save_path='save'):
        self.epoch = []
        self.history = defaultdict(list)
        self.SAVE_PATH = save_path
        self.acc_type = acc_type

    def on_epoch_end(self, epoch, logs={}):
        self.epoch.append(len(self.epoch))
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)
        with open('save/histories.p', 'wb') as f:
            pickle.dump(self.history, f)

    def _plot(self, name, metric):
        legend = [metric]
        plt.figure(figsize=(10, 6))
        plt.plot(self.epoch, self.history[metric], marker='.')

        val_metric = 'val_' + metric
        if val_metric in self.history:
            legend.append(val_metric)
            plt.plot(self.epoch, self.history[val_metric], marker='.')

        plt.title(name + ' over epochs')
        plt.xlabel('Epochs')
        plt.legend(legend, loc='best')

        if not os.path.exists(os.path.join(self.SAVE_PATH, 'images')):
            os.mkdir(os.path.join(self.SAVE_PATH, 'images'))

        filename = self.SAVE_PATH + '/images/' + name + '_epoch_' + str(len(self.epoch)) + '.png'
        plt.savefig(filename)

    def plot_loss(self):
        self._plot('Loss', 'loss')

    def plot_accuracy(self):
        # acc = 'acc'
        self._plot('Accuracy', self.acc_type)
