from flask import Flask, render_template, request
from flask import jsonify

import sys
import re
import numpy as np
from collections import Counter
from model.accent_models import *
import time

sys.path.append("python3/VietnameseDiacriticsRestoration")
from dictionary import *
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
tf.Session(config=config)

from VnKit.DataProcessor import DataProcessor

dp = DataProcessor()

NGRAM = 5

opt = {
    "use_ngrams": False,
    "use_model": "NEWS"
}

if opt["use_ngrams"]:
    MAX_SEQUENCE_LENGTH = 32
else:
    MAX_SEQUENCE_LENGTH = 100
BATCH_SIZE = 128
HIDDEN_SIZE = 512

if opt["use_model"] == "NEWS":
    MODEL = 'news_birnn_attention_512'
elif opt["use_model"] == "BOOKS":
    MODEL = 'book_birnn_attention_512'
else:
    MODEL = 'news_birnn_attention_512'

SAVE_PATH = 'save/' + MODEL

MODEL_NAME = SAVE_PATH + '/accent.h5'

CASE_SENSITIVE = False
EMBEDDING = False
# select dictionary
# Full Character
MODE_CHAR = 0
MODE_SUB_WORD = 1
MODE_SYLLABLE = 2
SELECT_MODE = MODE_CHAR

if SELECT_MODE == MODE_CHAR:
    if CASE_SENSITIVE:
        i_key2index = case_sensitive_base_char2index
        i_index2key = case_sensitive_index2base_char
        o_key2index = case_sensitive_full_char2index
        o_index2key = case_sensitive_index2full_char
    else:
        i_key2index = base_char2index
        i_index2key = index2base_char
        o_key2index = full_char2index
        o_index2key = index2full_char

INPUT_DIM = len(i_key2index)
OUTPUT_DIM = len(o_key2index)

white_space_letter = i_key2index[' ']
print("INPUT_DIM", INPUT_DIM)
print("OUTPUT_DIM", OUTPUT_DIM)
print("White space letter", white_space_letter)
print("Input 0xx", i_key2index['\x00'])
print("Output 0xx", o_key2index['\x00'])

# Load model
model = seq2seq_attention(max_sequence_length=MAX_SEQUENCE_LENGTH, input_dim=INPUT_DIM,
                          output_dim=OUTPUT_DIM, hidden_size=HIDDEN_SIZE)
model.load_weights(MODEL_NAME)


def read_test_sequence(sequence, max_len, show=False):
    invert = False
    input = []
    sequence = sequence.replace('_', ' ')
    new_sequence = []
    if SELECT_MODE == MODE_CHAR:
        new_sequence.append('<sos>')
        syllables = sequence.split(' ')
        syllables_len = len(syllables)
        for i, s in enumerate(syllables):
            if s.startswith('<') and s.endswith('>'):
                new_sequence.append(s)
            else:
                new_sequence.extend(list(s))
            if i < syllables_len - 1:
                new_sequence.append(' ')

        new_sequence.append('<eos>')

        if show == True:
            print(new_sequence)
        # Input
        if max_len > len(new_sequence):
            new_sequence = new_sequence + ['\x00'] * (max_len - len(new_sequence))
        else:
            new_sequence = new_sequence[:max_len]

        for c in new_sequence:
            if c in i_index2key:
                input.append(i_key2index[c])
            else:
                input.append(i_key2index['<oov>'])

    if invert:
        input = input[::-1]
    return input


def gen_ngrams(words, n=5):
    words = words.split(' ')
    ngrams = []
    if len(words) < n:
        ngram = u' '.join(words)
        ngrams.append(ngram)
    else:
        for i in range(len(words) - n + 1):
            ngram = u' '.join(words[i: i + n])
            ngrams.append(ngram)
    return ngrams


def read_phrases(path):
    with open(path, 'r', encoding="utf8") as f:
        lines = f.read().split('\n')

    print("Number of lines: ", len(lines))
    phrases = []
    for line in lines:
        words = line.split(' ')
        if words[-1] in PUNCTUATIONS:
            words = words[0:-1]
        phrase = []
        for word in words:
            if word == u'-':
                pass
            elif word in u',;:' and len(phrase) > 0:
                phrases.append(u' '.join(phrase))
                phrase = []
            else:
                phrase.append(word)
        if len(phrase) > 0:
            phrases.append(u' '.join(phrase))

    return phrases


def read_ngrams(phrases):
    ngrams = []
    for phrase in phrases:
        ngrams.extend(gen_ngrams(phrase, NGRAM))
    print("Number of ngrams:", len(ngrams))
    return ngrams


def encode_to_one_hot(sequence, vocab_size, max_len=MAX_SEQUENCE_LENGTH):
    if max_len:
        length = max_len
    else:
        length = len(sequence)

    if max_len == 1:
        X = np.zeros((vocab_size))
        X[sequence] = 1
    else:
        X = np.zeros((length, vocab_size))
        for i, c in enumerate(sequence[:length]):
            X[i, sequence[i]] = 1
    return X


def decode_input_one_hot(sequence_one_hot):
    output = []
    for c in sequence_one_hot:
        if c != 0:
            output.append(o_index2key[c])
    return output


def decode_output_one_hot(sequence_one_hot):
    output = []
    for c in sequence_one_hot:
        if c != 0:
            output.append(o_index2key[c])

    return output


def is_words(text):
    return re.fullmatch('\w[\w ]*', text)


def add_accent(text, use_ngrams=True):
    # print("--Add_accent")
    # Preprocessing data
    text = dp.process_data(text, tagging=True)
    text = dp.process_data_step_2(text, tagging=True)
    is_uppercase_map = [c.isupper() for c in text]
    text = remove_accent(text.lower())
    outputs = []

    words = text.split(' ')
    print(words)
    phrase = []
    for word in words:
        if word == u'-':
            pass
        elif word in u'.?!,;:' and len(phrase) > 0:
            print(phrase)
            outputs.append(_add_accent(u' '.join(phrase), use_ngrams))
            outputs.append(' ' + word + ' ')
            phrase = []
        else:
            phrase.append(word)
    if len(phrase) > 0:
        outputs.append(_add_accent(u' '.join(phrase), use_ngrams))
    print(phrase)
    # words_or_symbols_list = re.findall('\w[\w ]*|\W+', text)
    # # print('input:', words_or_symbols_list)
    # for words_or_symbols in words_or_symbols_list:
    #     # print(words_or_symbols)
    #     if is_words(words_or_symbols):
    #         outputs.append(_add_accent(words_or_symbols, use_ngrams))
    #     else:
    #         outputs.append(words_or_symbols)
    # print('output:', outputs)
    output_text = ''.join(outputs)

    # restore uppercase characters
    output_text = ''.join(c.upper() if is_upper else c
                          for c, is_upper in zip(output_text, is_uppercase_map))
    # print("--End add_accent")
    return output_text


def predict(samples):
    # print("--Predict samples")
    size = len(samples)
    # print(size)
    input_data = np.zeros([size, MAX_SEQUENCE_LENGTH, INPUT_DIM], dtype='float32')

    for i in range(size):
        # print(samples[i])
        input_data[i] = encode_to_one_hot(read_test_sequence(samples[i], MAX_SEQUENCE_LENGTH), INPUT_DIM,
                                          MAX_SEQUENCE_LENGTH)
        # print(input_data[i])
    # print(input_data.shape)

    out = model.predict(input_data)
    # print(out)
    predicts = []
    for i in range(size):
        predict_labels = list(np.argmax(out[i, :], 1))
        labels = decode_output_one_hot(predict_labels)
        if labels[0] == '<sos>':
            labels = labels[1:]
        if labels[-1] == '<eos>':
            labels = labels[0:-1]
        labels = ''.join(labels)
        predicts.append(labels)
        # print(labels)
    # print("--End predict samples")
    return predicts


def _add_accent(text, use_ngrams=True):
    # print("--_add_accent")
    # use ngram
    if use_ngrams:
        ngrams = gen_ngrams(text, n=NGRAM)
        # print(ngrams)
        predicted_ngrams = predict(ngrams)

        candidates = [Counter() for _ in range(len(predicted_ngrams) + NGRAM - 1)]
        for idx, gram in enumerate(predicted_ngrams):
            for wid, word in enumerate(re.split(' +', gram)):
                candidates[idx + wid].update([word])

        # print(candidates)
        output = ' '.join(c.most_common(1)[0][0] for c in candidates if c)
        # print(output)
        # print("--END _add_accent")
        return output
    # don't use ngram
    else:
        text = [text]
        predicted_text = predict(text)
        predicted_text = ''.join(predicted_text)
        # print(predicted_text)
        return predicted_text


app = Flask(__name__, static_url_path="/static")


# Routing
@app.route('/accented', methods=['GET'])
def accented():
    start = time.time()
    text = request.args.get('text')
    print("---Tieng Viet khong dau---")
    print(remove_accent(text))
    accented_text = add_accent(text, opt["use_ngrams"])
    print("---Tiếng Việt có dấu---")
    print(accented_text)
    end = time.time()
    return jsonify({"original": text, 'with_accent': accented_text,
                    'took': float(end - start)})


@app.route('/remove', methods=['GET'])
def remove():
    text = request.args.get('text')
    remove_accent_text = remove_accent(text)
    return jsonify({"remove_accent_text": remove_accent_text})


@app.route("/")
def index():
    return render_template("index.html")


# start app
if (__name__ == "__main__"):
    app.run(port=5000)
