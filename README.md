# Vietnamese Diacritics Restoration
Update at [https://gitlab.com/bachhovuong310/VietnameseDiacriticsRestoration]
## Preprocessing data
Using VnToolKit [https://gitlab.com/bachhovuong310/VNToolKit]

## Models
1. Encoder - Decoder
2. Bidrirectional Encoder - Decoder - Attention

Level : Character Level

## Demo
python demo.py --example "Chung em sinh hoat o nha van hoa vao dip he." --use_ngrams

python demo.py --example "Toi thay hoa vang tren co xanh"

python demo.py --example "Hom nay troi dep qua"

## Training
python accent.py