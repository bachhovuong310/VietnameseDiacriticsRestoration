from model.accent_models import *
from EverHistory import EverHistory
from keras.callbacks import ModelCheckpoint
import sys
import os

sys.path.append("python3/VietnameseDiacriticsRestoration")
from dictionary import *
import pandas as pd
import matplotlib.pyplot as plt
import itertools
import numpy as np
import random
import pickle

random.seed(1337)
np.random.seed(1337)

# CONFIG
TRAIN_CONTINUE = False

NGRAM = 5
MAX_SEQUENCE_LENGTH = 80
BATCH_SIZE = 128
HIDDEN_SIZE = 512

TRAIN = 0
EVALUATE = 1
# TRAIN_OR_EVALUATE = TRAIN
TRAIN_OR_EVALUATE = EVALUATE

# MODEL = 'encoder_decoder_512'
MODEL = 'book_birnn_attention_512'

NEWS = 0
BOOKS = 1
# NEWS_OR_BOOKS_CORPUS = BOOKS
NEWS_OR_BOOKS_CORPUS = NEWS

if NEWS_OR_BOOKS_CORPUS == BOOKS:
    INPUT_FILE = 'data/books_corpus.txt'
else:
    INPUT_FILE = 'data/news_corpus.txt'

SAVE_PATH = 'save/' + MODEL
if not os.path.exists(SAVE_PATH):
    os.mkdir(SAVE_PATH)

MODEL_NAME = SAVE_PATH + '/accent.h5'

CASE_SENSITIVE = False
EMBEDDING = False
# select dictionary
# Full Character
MODE_CHAR = 0
MODE_SUB_WORD = 1
MODE_SYLLABLE = 2
SELECT_MODE = MODE_CHAR

if SELECT_MODE == MODE_CHAR:
    if NEWS_OR_BOOKS_CORPUS == BOOKS:
        CORPUS_PATH = 'data/books_char.pkl'
    else:
        CORPUS_PATH = 'data/news_char.pkl'

    if CASE_SENSITIVE:
        i_key2index = case_sensitive_base_char2index
        i_index2key = case_sensitive_index2base_char
        o_key2index = case_sensitive_full_char2index
        o_index2key = case_sensitive_index2full_char
    else:
        i_key2index = base_char2index
        i_index2key = index2base_char
        o_key2index = full_char2index
        o_index2key = index2full_char

INPUT_DIM = len(i_key2index)
OUTPUT_DIM = len(o_key2index)

white_space_letter = i_key2index[' ']
print("INPUT_DIM", INPUT_DIM)
print("OUTPUT_DIM", OUTPUT_DIM)
print("White space letter", white_space_letter)
print("Input 0xx", i_key2index['\x00'])
print("Output 0xx", o_key2index['\x00'])


def read_sequence(output_sequence, max_len, show=False):
    invert = False
    input = []
    output = []
    output_sequence = output_sequence.replace('_', ' ')
    if SELECT_MODE == MODE_CHAR:
        new_input_sequence = ['<sos>']
        new_output_sequence = ['<sos>']

        syllables = output_sequence.split(' ')
        syllables_len = len(syllables)
        for i, s in enumerate(syllables):
            if is_syllable(s.lower()):
                s = s.lower()
                new_input_sequence.extend(list(remove_accent(s)))
                new_output_sequence.extend(list(s))
            else:
                new_input_sequence.append(s)
                new_output_sequence.append(s)

            if i < syllables_len - 1:
                new_input_sequence.append(' ')
                new_output_sequence.append(' ')

        new_input_sequence.append('<eos>')
        new_output_sequence.append('<eos>')

        if show == True:
            print(new_input_sequence)
            print(new_output_sequence)
        # Input
        if max_len > len(new_input_sequence):
            new_input_sequence = new_input_sequence + ['\x00'] * (max_len - len(new_input_sequence))
        else:
            new_input_sequence = new_input_sequence[:max_len]

        for c in new_input_sequence:
            if c in i_index2key:
                input.append(i_key2index[c])
            else:
                input.append(i_key2index['<oov>'])

        # Output
        if max_len > len(new_output_sequence):
            new_output_sequence = new_output_sequence + ['\x00'] * (max_len - len(new_output_sequence))
        else:
            new_output_sequence = new_output_sequence[:max_len]

        for c in new_output_sequence:
            if c in o_index2key:
                output.append(o_key2index[c])
            else:
                output.append(o_key2index['<oov>'])

    if invert:
        input = input[::-1]
    return input, output


def gen_ngrams(words, n=3):
    words = words.split(' ')
    ngrams = []
    if len(words) < n:
        ngram = u' '.join(words)
        ngrams.append(ngram)
    else:
        for i in range(len(words) - n + 1):
            ngram = u' '.join(words[i: i + n])
            ngrams.append(ngram)
    return ngrams


def read_phrases(path):
    with open(path, 'r', encoding="utf8") as f:
        lines = f.read().split('\n')
    pd.DataFrame([len(line.split(' ')) for line in lines]).hist(bins=20)
    plt.savefig('save/lines.png')
    print("Number of lines: ", len(lines))
    phrases = []
    for line in lines:
        words = line.split(' ')
        if len(words) <= 3:
            continue

        if words[-1] in PUNCTUATIONS:
            words = words[0:-1]
        phrase = []
        for word in words:
            if word == u'-':
                pass
            elif word in u',;:' and len(phrase) > 0:
                phrases.append(u' '.join(phrase))
                phrase = []
            else:
                phrase.append(word)
        if len(phrase) > 0:
            phrases.append(u' '.join(phrase))

    pd.DataFrame([len(p.split(' ')) for p in phrases]).hist(bins=20)
    print("Number of phrases: ", len(phrases))
    plt.savefig('save/phrases_by_word.png')
    # pd.DataFrame([len(split_syllables(p)) for p in phrases]).hist(bins=20)
    # plt.savefig('save/phrases_by_syllables.png')
    pd.DataFrame([len(p) for p in phrases]).hist(bins=20)
    plt.savefig('save/phrases_by_char.png')
    # new_phrases = []
    # for phrase in phrases:
    #     if len(phrase.split(' ')) >= 2:
    #         new_phrases.append(phrase)
    # return new_phrases
    return phrases


def read_ngrams(phrases):
    ngrams = []
    for phrase in phrases:
        ngrams.extend(gen_ngrams(phrase, NGRAM))
    print("Number of ngrams:", len(ngrams))
    pd.DataFrame([len(ngram) for ngram in ngrams]).hist(bins=20)
    plt.savefig('save/ngrams_by_char.png')
    return ngrams


def read_data(path):
    full_data = []
    if os.path.exists(CORPUS_PATH):
        with open(CORPUS_PATH, 'rb') as f:
            full_data = pickle.load(f)
    else:
        data = read_phrases(path)
        full_data = []

        for d in data[:10]:
            print('-' * 50)
            print(d)
            i, o = read_sequence(d, MAX_SEQUENCE_LENGTH, show=True)
            print(i)
            print(o)
        for i, s in enumerate(data):
            full_data.append(read_sequence(s, MAX_SEQUENCE_LENGTH))
            if i % 10000 == 0:
                print(i)

        with open(CORPUS_PATH, 'wb') as f:
            pickle.dump(full_data, f)

    return full_data


def gen_batch(it, size):
    for _, group in itertools.groupby(enumerate(it), lambda x: x[0] // size):
        yield list(zip(*group))[1]


def encode_to_one_hot(sequence, vocab_size, max_len=MAX_SEQUENCE_LENGTH):
    length = len(sequence)

    if max_len == 1:
        X = np.zeros((vocab_size))
        X[sequence] = 1
    else:
        X = np.zeros((length, vocab_size))
        for i, c in enumerate(sequence[:length]):
            X[i, sequence[i]] = 1
    return X


def gen_stream(data, has_encoded=True):
    while True:
        for i in range(len(data)):
            input_vec, output_vec = data[i]
            if has_encoded == True:
                if not EMBEDDING:
                    input_vec = encode_to_one_hot(input_vec, INPUT_DIM, max_len=MAX_SEQUENCE_LENGTH)
                output_vec = encode_to_one_hot(output_vec, OUTPUT_DIM, max_len=MAX_SEQUENCE_LENGTH)
            # print(input_vec,output_vec)
            if input_vec is not None and output_vec is not None:
                yield input_vec, output_vec


def gen_data(data, batch_size=64, has_encoded=True):
    for batch in gen_batch(gen_stream(data, has_encoded), size=batch_size):
        X, Y = zip(*batch)
        yield np.array(X), np.array(Y)


def evaluate_data(model, test_data, show=False):  # data la cap input, output
    nb_batches = len(test_data) // BATCH_SIZE - 1

    true_char_count = 0
    total_char = 0
    true_word_count = 0
    total_word = 0

    for batch in range(nb_batches):
        print("Batch", batch)
        batch_data = test_data[BATCH_SIZE * batch: BATCH_SIZE * (batch + 1)]
        batch_input = np.zeros([BATCH_SIZE, MAX_SEQUENCE_LENGTH, INPUT_DIM], dtype='float32')

        for index in range(BATCH_SIZE):
            input, output = batch_data[index]
            batch_input[index] = encode_to_one_hot(input, INPUT_DIM, MAX_SEQUENCE_LENGTH)

        predict = model.predict(batch_input)

        word_is_correct = True

        for index in range(BATCH_SIZE):
            input, output = batch_data[index]
            # print("Batch", batch, " Index", index)
            predict_labels = list(np.argmax(predict[index, :], 1))
            # if show:
            #     print(input)
            #     print(output)
            #     print(predict_labels)
            #     input_text = decode_input_one_hot(input)
            #     output_text = decode_output_one_hot(output)
            #     predict_text = decode_output_one_hot(predict_labels)
            #     print("Input:", ''.join(input_text))
            #     print("True Output:", ''.join(output_text))
            #     print("Predict Output:", ''.join(predict_text))

            for index in range(MAX_SEQUENCE_LENGTH):
                if input[index] == 0:
                    break
                total_char += 1

                # Kiem tra tu
                if input[index] == 1:
                    total_word += 1
                    if word_is_correct:
                        true_word_count += 1
                    else:
                        word_is_correct = True

                if output[index] != predict_labels[index]:
                    word_is_correct = False
                else:
                    true_char_count += 1

            # Tu cuoi cung trong chuoi
            if word_is_correct:
                true_word_count += 1
            else:
                word_is_correct = True

            total_word += 1

    char_accuracy = true_char_count / total_char
    word_accuracy = true_word_count / total_word

    print("Char accuracy:", char_accuracy)
    print("Word accuracy:", word_accuracy)
    return char_accuracy, word_accuracy


def evaluate_data_generator(model, test_generator, nb_batches=1000):  # data la cap input, output
    true_char_count = 0
    total_char = 0
    true_word_count = 0
    total_word = 0

    for batch in range(nb_batches):
        print("Batch", batch)
        inputs, outputs = next(test_generator)

        batch_input = np.zeros([BATCH_SIZE, MAX_SEQUENCE_LENGTH, INPUT_DIM], dtype='float32')

        for index in range(BATCH_SIZE):
            input = inputs[index]
            batch_input[index] = encode_to_one_hot(input, INPUT_DIM, MAX_SEQUENCE_LENGTH)

        # Predict
        predict = model.predict(batch_input)

        word_is_correct = True

        for index in range(BATCH_SIZE):
            input, output = inputs[index], outputs[index]
            # print("Batch", batch, " Index", index)
            predict_labels = list(np.argmax(predict[index, :], 1))
            # if show:
            #     print(input)
            #     print(output)
            #     print(predict_labels)
            #     input_text = decode_input_one_hot(input)
            #     output_text = decode_output_one_hot(output)
            #     predict_text = decode_output_one_hot(predict_labels)
            #     print("Input:", ''.join(input_text))
            #     print("True Output:", ''.join(output_text))
            #     print("Predict Output:", ''.join(predict_text))

            for index in range(MAX_SEQUENCE_LENGTH):
                if input[index] == 0:
                    break
                total_char += 1

                # Kiem tra tu
                if input[index] == 1:
                    total_word += 1
                    if word_is_correct:
                        true_word_count += 1
                    else:
                        word_is_correct = True

                if output[index] != predict_labels[index]:
                    word_is_correct = False
                else:
                    true_char_count += 1

            # Tu cuoi cung trong chuoi
            if word_is_correct:
                true_word_count += 1
            else:
                word_is_correct = True

            total_word += 1

    char_accuracy = true_char_count / total_char
    word_accuracy = true_word_count / total_word

    print("Char accuracy:", char_accuracy)
    print("Word accuracy:", word_accuracy)
    return char_accuracy, word_accuracy


# text = 'tiểu_sử nhà_văn nguyễn_nhật_ánh'
# i, o = read_sequence(text, MAX_SEQUENCE_LENGTH)
# print(i)
# print(o)
# print(encode_to_one_hot(i, INPUT_DIM))
# print(encode_to_one_hot(o, OUTPUT_DIM))
print("PREPARE DATA.......")
full_data = read_data(INPUT_FILE)
# full_data = full_data[0:10240]
VALIDATION_SIZE = int(len(full_data) / 10)
TRAIN_SIZE = len(full_data) - VALIDATION_SIZE

train_data = full_data[:TRAIN_SIZE]
validation_data = full_data[TRAIN_SIZE:]

train_generator = gen_data(train_data, batch_size=BATCH_SIZE)
validation_generator = gen_data(validation_data, batch_size=BATCH_SIZE)

test_generator = gen_data(validation_data, batch_size=BATCH_SIZE, has_encoded=False)

print("Number of train samples:", TRAIN_SIZE)
print("BUILD MODEL........")

steps_per_epoch = TRAIN_SIZE // BATCH_SIZE
validation_steps = VALIDATION_SIZE // BATCH_SIZE
# steps_per_epoch = 10000
# validation_steps = 1000
validation_steps_1 = 1

# model = seq2seq(max_sequence_length=MAX_SEQUENCE_LENGTH, input_dim=INPUT_DIM,
#                output_dim=OUTPUT_DIM, hidden_size=HIDDEN_SIZE)
if EMBEDDING:
    model = embedding_seq2seq_attention(max_sequence_length=MAX_SEQUENCE_LENGTH, input_dim=INPUT_DIM,
                                        output_dim=OUTPUT_DIM, hidden_size=HIDDEN_SIZE)
else:
    model = seq2seq_attention(max_sequence_length=MAX_SEQUENCE_LENGTH, input_dim=INPUT_DIM,
                              output_dim=OUTPUT_DIM, hidden_size=HIDDEN_SIZE)

if TRAIN_OR_EVALUATE == TRAIN:
    if TRAIN_CONTINUE == True:
        model.load_weights(MODEL_NAME)

    # Save
    history = EverHistory('acc', SAVE_PATH)
    file_path = SAVE_PATH + '/weights-improvement-{epoch:02d}-{val_acc:.4f}-{val_loss:.4f}-bigger.hdf5'
    checkpoint = ModelCheckpoint(file_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')

    from IPython import display

    print("TRAIN MODEL.........")

    NB_EPOCHS = 60

    char_accuracies = []
    word_accuracies = []

    for iteration in range(NB_EPOCHS):
        print('-' * 50)
        print('Iteration: ', iteration)
        model.fit_generator(train_generator, epochs=1, steps_per_epoch=steps_per_epoch,
                            validation_data=validation_generator, validation_steps=validation_steps_1,
                            callbacks=[history, checkpoint])

        display.clear_output(wait=True)
        char_acc, word_acc = evaluate_data(model, validation_data)
        # char_acc, word_acc = evaluate_data_generator(model, test_generator, validation_steps)
        char_accuracies.append(char_acc)
        word_accuracies.append(word_acc)
        with open(os.path.join(SAVE_PATH, 'char_accuracies.pkl'), 'wb') as f_char:
            pickle.dump(char_accuracies, f_char)
        with open(os.path.join(SAVE_PATH, 'word_accuracies.pkl'), 'wb') as f_word:
            pickle.dump(word_accuracies, f_word)

        if iteration % 2 == 0:
            history.plot_loss()
            history.plot_accuracy()
        model.save(MODEL_NAME)
else:
    model.load_weights(MODEL_NAME)
    # char_acc, word_acc = evaluate_data_generator(model, test_generator, validation_steps)
    evaluate_data(model, validation_data)
