from keras.models import Model
from keras.layers import Input, RepeatVector, TimeDistributed, Dense, Dropout, recurrent, Activation, Embedding, \
    Bidirectional


# Encoder Decoder
def SEQ2SEQ(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
    RNN = recurrent.LSTM
    input = Input(shape=(max_sequence_length, input_dim), dtype='float32')
    x = RNN(hidden_size)(input)
    # x = Bidirectional(LSTM(hidden_size), merge_mode="sum")(input)
    x = RepeatVector(max_sequence_length)(x)
    x = RNN(hidden_size, return_sequences=True)(x)
    x = RNN(hidden_size, return_sequences=True)(x)
    x = Dropout(0.25)(x)
    output = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(input, output)
    # optimizer = SGD(lr=0.001, momentum=0.9, decay=1e-6, nesterov=True)

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['acc'])
    model.summary()
    return model

# def model_1(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
#     RNN = recurrent.LSTM
#     input = Input(shape=(max_sequence_length, input_dim), dtype='float32')
#     x = Bidirectional(RNN(hidden_size // 2), merge_mode="concat")(input)
#     x = RepeatVector(max_sequence_length)(x)
#     x = RNN(hidden_size, return_sequences=True)(x)
#     x = RNN(hidden_size, return_sequences=True)(x)
#     x = Dropout(0.25)(x)
#     output = TimeDistributed(Dense(output_dim, activation='softmax'))(x)
#
#     model = Model(input, output)
#     # optimizer = SGD(lr=0.001, momentum=0.9, decay=1e-6, nesterov=True)
#
#     model.compile(loss='categorical_crossentropy',
#                   optimizer='adam',
#                   metrics=['acc'])
#     model.summary()
#     return model


# def model_2(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
#     RNN = recurrent.LSTM
#     embedding_size = 15
#     input = Input(shape=(max_sequence_length,), dtype='int32', name='input')
#     x = Embedding(input_dim=input_dim, output_dim=embedding_size, input_length=max_sequence_length, name='embedding',
#                   mask_zero=True)(input)
#     # x = RNN(hidden_size, name='rnn_1')(x)
#     x = Bidirectional(RNN(hidden_size, name='rnn_1'), merge_mode='concat')(x)
#     x = RepeatVector(max_sequence_length, name='repeat_vector_1')(x)
#     x = RNN(hidden_size, return_sequences=True, name='rnn_2')(x)
#     x = RNN(hidden_size, return_sequences=True, name='rnn_3')(x)
#     x = Dropout(0.25, name='dropout')(x)
#     output = Dense(output_dim, activation='softmax', name='softmax')(x)
#
#     model = Model(input, output)
#
#     model.compile(loss='categorical_crossentropy',
#                   optimizer='adam',
#                   metrics=['acc'])
#     model.summary()
#     return model
