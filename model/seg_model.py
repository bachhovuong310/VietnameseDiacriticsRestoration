from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Dropout, Activation, Embedding
from keras.optimizers import Adam
from .sequence_blocks import *


def seq2seq_attention(max_sequence_length, input_dim, output_dim, hidden_size):
    # Input Block
    i = Input(shape=(max_sequence_length, input_dim), dtype='float32')
    # Encoder Block
    x = Encoder(hidden_size, return_sequences=True, bidirectional=True, merge_mode='concat')(i)
    attention = Maxpool(x)
    # Decoder Block
    x = AttentionDecoder(hidden_size, return_sequences=True, bidirectional=True)(x, attention)
    x = Dropout(0.25)(x)
    o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(inputs=i, outputs=o)
    opt = Adam(lr=0.001, clipvalue=1.)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    return model


def embedding_seq2seq_attention(max_sequence_length, input_dim, output_dim, hidden_size):
    # Input Block
    i = Input(shape=(max_sequence_length, ), dtype='int32')
    embedding_size = 300
    x = Embedding(input_dim=input_dim, output_dim=embedding_size, input_length=max_sequence_length, mask_zero=True)(i)
    # Encoder Block
    x = Encoder(hidden_size, return_sequences=True, bidirectional=True, merge_mode='concat')(x)
    attention = Maxpool(x)
    # Decoder Block
    x = AttentionDecoder(hidden_size, return_sequences=True, bidirectional=True)(x, attention)
    x = Dropout(0.25)(x)
    o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(inputs=i, outputs=o)
    opt = Adam(lr=0.001, clipvalue=1.)
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['binary_accuracy'])
    model.summary()
    return model

# def seg_model(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
#     RNN = recurrent.LSTM
#     # embedding_size = 300
#     embedding_size = input_dim // 5
#     input = Input(shape=(max_sequence_length,), dtype='int32', name='input')
#     x = Embedding(input_dim=input_dim, output_dim=embedding_size, input_length=max_sequence_length, name='embedding',
#                   mask_zero=True)(input)
#     x = RNN(hidden_size, name='rnn_1')(x)
#     x = RepeatVector(max_sequence_length, name='repeat_vector_1')(x)
#     x = RNN(hidden_size, return_sequences=True, name='rnn_2')(x)
#     # x = RNN(hidden_size, return_sequences=True, name='rnn_3')(x)
#     x = Dropout(0.25, name='dropout')(x)
#     output = Dense(output_dim, activation='softmax', name='softmax')(x)
#
#     model = Model(input, output)
#
#     model.compile(loss='categorical_crossentropy',
#                   optimizer='adam',
#                   metrics=['acc'])
#     model.summary()
#     return model
#
#
# def seg_model_2(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
#     embedding_dim = 300
#     # embedding_dim = input_dim // 5
#     RNN = recurrent.LSTM
#     input = Input(shape=(max_sequence_length,), dtype='int32', name='input')
#     x = Embedding(input_dim=input_dim, output_dim=embedding_dim, input_length=max_sequence_length, name='embedding',
#                   mask_zero=True)(input)
#     x = Bidirectional(RNN(hidden_size, name='rnn_1'), merge_mode='concat')(x)
#
#     x = RepeatVector(max_sequence_length, name='repeat_vector_1')(x)
#     x = RNN(hidden_size, return_sequences=True, name='rnn_2')(x)
#     x = Dropout(0.25, name='dropout')(x)
#     output = TimeDistributed(Dense(output_dim, activation='softmax', name='softmax'))(x)
#
#     model = Model(input, output)
#
#     # model.compile(loss='categorical_crossentropy',
#     #               optimizer='adam',
#     #               metrics=['acc'])
#
#     model.compile(loss='binary_crossentropy',
#                   optimizer='adam',
#                   metrics=['binary_accuracy'])
#     model.summary()
#     return model
#
#
# def seg_model_3(max_sequence_length=32, input_dim=40, output_dim=40, hidden_size=128):
#     embedding_dim = 300
#     # embedding_dim = input_dim // 5
#     RNN = recurrent.LSTM
#     input = Input(shape=(max_sequence_length,), dtype='int32', name='input')
#     x = Embedding(input_dim=input_dim, output_dim=embedding_dim, input_length=max_sequence_length, name='embedding',
#                   mask_zero=True)(input)
#     x = Bidirectional(RNN(hidden_size, return_sequences=True, name='rnn_1'), merge_mode='sum')(x)
#     output = Dense(output_dim, activation='softmax', name='softmax')(x)
#     model = Model(input, output)
#
#     model.compile(loss='categorical_crossentropy',
#                   optimizer='adam',
#                   metrics=['acc'])
#
#     # model.compile(loss='binary_crossentropy',
#     #               optimizer='adam',
#     #               metrics=['binary_accuracy'])
#     model.summary()
#     return model
