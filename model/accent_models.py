from keras.models import Model
from keras import backend as K
from keras.layers import Masking, Input, Dropout, LSTM, GRU, Dense, RepeatVector, TimeDistributed, Embedding
from .sequence_blocks import *
from keras.optimizers import Adam


def seq2seq(max_sequence_length, input_dim, output_dim, hidden_size, use_gru=False):
    # Input Block
    input = Input(shape=(max_sequence_length, input_dim), dtype='float32')
    x = Masking(mask_value=0.)(input)
    # Encoder Block
    x = Encoder(hidden_size // 2, return_sequences=False, bidirectional=True, merge_mode='concat')(x)
    # x = Encoder(hidden_size, return_sequences=False, use_gru=use_gru)(x)
    # x = Dropout(.5)(x)
    x = RepeatVector(max_sequence_length)(x)
    # Decoder Block
    x = Decoder(hidden_size, return_sequences=True)(x)
    x = Dropout(.25)(x)
    x = Decoder(hidden_size, return_sequences=True)(x)
    x = Dropout(.25)(x)

    o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(input, o)

    opt = Adam(lr=0.0005, clipvalue=1., decay=1e-7)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    return model


def seq2seq_attention(max_sequence_length, input_dim, output_dim, hidden_size):
    # Input Block
    i = Input(shape=(max_sequence_length, input_dim), dtype='float32')
    x = Masking(mask_value=0.)(i)
    # Encoder Block
    x = Encoder(hidden_size // 2, return_sequences=True, bidirectional=True, merge_mode='concat')(x)
    attention = Maxpool(x)
    # Decoder Block
    # x = AttentionDecoder(hidden_size // 2, return_sequences=True, bidirectional=True)(x, attention)
    x = AttentionDecoder(hidden_size, return_sequences=True)(x, attention)
    x = Dropout(0.25)(x)
    o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(inputs=i, outputs=o)
    decay = 1e-7
    # lr = 0.0005 - 5 * decay
    opt = Adam(lr=0.0005, clipvalue=1., decay=decay)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    return model


def embedding_seq2seq_attention(max_sequence_length, input_dim, output_dim, hidden_size):
    # Input Block
    i = Input(shape=(max_sequence_length,), dtype='int32')
    embedding_size = input_dim
    x = Embedding(input_dim=input_dim, output_dim=embedding_size, input_length=max_sequence_length, mask_zero=True)(i)
    # Encoder Block
    x = Encoder(hidden_size // 2, return_sequences=True, bidirectional=True, merge_mode='concat')(x)
    attention = Maxpool(x)
    # Decoder Block
    x = AttentionDecoder(hidden_size // 2, return_sequences=True, bidirectional=True)(x, attention)
    x = Dropout(0.25)(x)
    o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)

    model = Model(inputs=i, outputs=o)
    opt = Adam(lr=0.001, clipvalue=1., decay=1e-7)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    return model

# def seq2seq_attention(max_sequence_length, input_dim,output_dim, hidden_size, use_gru=False):
#     # Input Block
#     i = Input(shape=(max_sequence_length, input_dim) ,dtype='float32')
#     # Encoder Block
#     x = Encoder(hidden_size, return_sequences=True, bidirectional=True, merge_mode='sum', use_gru=use_gru)(i)
#     x = Dropout(0.25)(x)
#     x = Encoder(hidden_size, return_sequences=True, bidirectional=True, merge_mode='concat', use_gru=use_gru)(x)
#     x = Dropout(0.25)(x)
#     # x = TimeDistributed(Dense(hidden_size, activation='linear'))(x)
#     # x = ELU()(x)
#     attention = Maxpool(x)
#     # Decoder Block
#     x = AttentionDecoder(hidden_size, return_sequences=True, bidirectional=True, use_gru=use_gru)(x, attention)
#     x = Dropout(0.25)(x)
#     x = AttentionDecoder(hidden_size, return_sequences=True, bidirectional=True, use_gru=use_gru)(x, attention)
#     x = Dropout(0.25)(x)
#     o = TimeDistributed(Dense(output_dim, activation='softmax'))(x)
#
#     model = Model(inputs=i, outputs=o)
#     opt = Adam(lr=0.0001, clipvalue=1.)
#     model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
#     model.summary()
#     return model
